## <span class="numero">{{ include.numero }}.</span> {{ include.title | default: "Dictionary of Terms" }} {#{% if include.id_prefix %}{{ include.id_prefix }}.{% endif %}dictionary}
{% assign subnumero = 0 %}{% assign terms = site.data[include.name]["@graph"] %}{% assign ontology = terms | where_exp: "item", 'item["@type"] contains "owl:Ontology"' | first %}{% assign ontology_prefix = include.ontology_prefix | default: ontology["@id"] %}{% assign classes = terms | where_exp: "item", 'item["@type"] contains "owl:Class"' | sort: "label" %}{% assign object_properties = terms | where_exp: "item", 'item["@type"] contains "owl:ObjectProperty"' | sort: "label" %}{% assign data_properties = terms | where_exp: "item", 'item["@type"] contains "owl:DatatypeProperty"' | sort: "label" %}{% assign annotation_properties = terms | where_exp: "item", 'item["@type"] contains "owl:AnnotationProperty"' | sort: "label" %}{% assign individuals = terms | where_exp: "item", 'item["@type"] contains "owl:NamedIndividual"' | sort: "label" %}

<nav id="#{% if include.id_prefix %}{{ include.id_prefix }}.{% endif %}dictionary.termlist" class="termlist" markdown="1">

{% if classes.size > 0 %}**Classes** <span markdown="1">{% for class in classes %}[{{ class["label"] }}](#{{ class["@id"] | remove_first: ontology_prefix }}){% if class["term status"] %}{:data-vs--term_status="{{ class["term status"] }}"}{% endif %}{% unless forloop.last %}&#x20;{% endunless %}{% endfor %}</span>

{% endif %}{% if object_properties.size > 0 %}**Object properties** <span markdown="1">{% for object_property in object_properties %}[{{ object_property["label"] }}](#{{ object_property["@id"] | remove_first: ontology_prefix }}){% if object_property["term status"] %}{:data-vs--term_status="{{ object_property["term status"] }}"}{% endif %}{% unless forloop.last %}&#x20;{% endunless %}{% endfor %}</span>

{% endif %}{% if data_properties.size > 0 %}**Data properties** <span markdown="1">{% for data_property in data_properties %}[{{ data_property["label"] }}](#{{ data_property["@id"] | remove_first: ontology_prefix }}){% if data_property["term status"] %}{:data-vs--term_status="{{ data_property["term status"] }}"}{% endif %}{% unless forloop.last %}&#x20;{% endunless %}{% endfor %}</span>

{% endif %}{% if annotation_properties.size > 0 %}**Annotation properties** <span markdown="1">{% for annotation_property in annotation_properties %}[{{ annotation_property["label"] }}](#{{ annotation_property["@id"] | remove_first: ontology_prefix }}){% if annotation_property["term status"] %}{:data-vs--term_status="{{ annotation_property["term status"] }}"}{% endif %}{% unless forloop.last %}&#x20;{% endunless %}{% endfor %}</span>

{% endif %}{% if individuals.size > 0 %}**Named Individuals** <span markdown="1">{% for individual in individuals %}[{{ individual["label"] }}](#{{ individual["@id"] | remove_first: ontology_prefix }}){% if individual["term status"] %}{:data-vs--term_status="{{ individual["term status"] }}"}{% endif %}{% unless forloop.last %}&#x20;{% endunless %}{% endfor %}</span>

{% endif %}</nav>

{% if classes.size > 0 %}{% assign subnumero = subnumero | plus: 1 %}{% assign subsubnumero = 0 %}### <span class="numero">{{ include.numero }}.{{ subnumero }}</span> Class Definitions
{:id="{% if include.id_prefix %}{{ include.id_prefix }}.{% endif %}dictionary.classes"}

{% for class in classes %}{% assign subsubnumero = subsubnumero | plus: 1 %}{% assign term_numero = include.numero | append: "." | append: subnumero | append: "." | append: subsubnumero %}{% include term.markdown vocabulary=include.name term=class kind="class" numero=term_numero prefix=ontology_prefix %}
{% endfor %}{% endif %}{% if object_properties.size > 0 %}{% assign subnumero = subnumero | plus: 1 %}{% assign subsubnumero = 0 %}### <span class="numero">{{ include.numero }}.{{ subnumero }}</span> Object Property Definitions
{:id="{% if include.id_prefix %}{{ include.id_prefix }}.{% endif %}dictionary.object_properties"}

{% for object_property in object_properties %}{% assign subsubnumero = subsubnumero | plus: 1 %}{% assign term_numero = include.numero | append: "." | append: subnumero | append: "." | append: subsubnumero %}{% include term.markdown vocabulary=include.name term=object_property kind="object_property" numero=term_numero prefix=ontology_prefix %}
{% endfor %}{% endif %}{% if data_properties.size > 0 %}{% assign subnumero = subnumero | plus: 1 %}{% assign subsubnumero = 0 %}### <span class="numero">{{ include.numero }}.{{ subnumero }}</span> Data Property Definitions
{:id="{% if include.id_prefix %}{{ include.id_prefix }}.{% endif %}dictionary.data_properties"}

{% for data_property in data_properties %}{% assign subsubnumero = subsubnumero | plus: 1 %}{% assign term_numero = include.numero | append: "." | append: subnumero | append: "." | append: subsubnumero %}{% include term.markdown vocabulary=include.name term=data_property kind="data_property" numero=term_numero prefix=ontology_prefix %}
{% endfor %}{% endif %}{% if annotation_properties.size > 0 %}{% assign subnumero = subnumero | plus: 1 %}{% assign subsubnumero = 0 %}### <span class="numero">{{ include.numero }}.{{ subnumero }}</span> Annotation Property Definitions
{:id="{% if include.id_prefix %}{{ include.id_prefix }}.{% endif %}dictionary.annotation_properties"}

{% for annotation_property in annotation_properties %}{% assign subsubnumero = subsubnumero | plus: 1 %}{% assign term_numero = include.numero | append: "." | append: subnumero | append: "." | append: subsubnumero %}{% include term.markdown vocabulary=include.name term=annotation_property kind="annotation_property" numero=term_numero prefix=ontology_prefix %}
{% endfor %}{% endif %}{% if individuals.size > 0 %}{% assign subnumero = subnumero | plus: 1 %}{% assign subsubnumero = 0 %}### <span class="numero">{{ include.numero }}.{{ subnumero }}</span> Named Individual Definitions
{:id="{% if include.id_prefix %}{{ include.id_prefix }}.{% endif %}dictionary.named_individuals"}

{% for individual in individuals %}{% assign subsubnumero = subsubnumero | plus: 1 %}{% assign term_numero = include.numero | append: "." | append: subnumero | append: "." | append: subsubnumero %}{% include term.markdown vocabulary=include.name term=individual kind="named_individual" numero=term_numero prefix=ontology_prefix %}
{% endfor %}{% endif %}
