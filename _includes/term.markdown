{% capture shortid %}{{ include.term["@id"] | remove_first: include.prefix }}{% endcapture %}<section id="{{ include.kind }}.{{ shortid }}"{% if include.term["term status"] %} data-vs--term_status="{{ include.term["term status"] }}"{% endif %} markdown="block">
#### <span class="numero">{{ include.numero }}</span> {% if include.term["term status"] %}<span style="Line-Height: 1">{% case include.term["term status"] %}{% when "stable" %}✅{% when "unstable" %}🚧{% when "archaic" %}🦖{% when "dependency" %}📥{% else %}🔬{% endcase %}</span> {% endif %}{{ include.term.label }} <small>(<code>{{ include.term["@id"] }}</code>)</small>
{:id="{{ shortid }}" .no_toc}
<span lang="en"><abbr title="Internationalized Resource Identifier">I·R·I</abbr></span>

: {% assign prefix = include.term["@id"] | split: ":" | first %}[`{{ site.data[include.vocabulary]["@context"][prefix] | default: site.data.include.context[prefix] | default: prefix }}{{ include.term["@id"] | remove_first: prefix | remove_first: ":" }}`]({{ site.data[include.vocabulary]["@context"][prefix] | default: site.data.include.context[prefix] | default: prefix }}{{ include.term["@id"] | remove_first: prefix | remove_first: ":" }})

{% if include.term["@type"] contains "owl:NamedIndividual" and include.term["@type"].size > 1 %}<span lang="en">Type</span>

{% for class_of_individual in include.term["@type"] %}{% unless class_of_individual == "owl:NamedIndividual" %}: {% include class.markdown term=class_of_individual %}

{% endunless %}{% endfor %}{% endif %}{% if include.term["equivalent class"] %}<span lang="en">Equivalent Class</span>

{% for equivalent in include.term["equivalent class"] %}: {% include class.markdown term=equivalent %}

{% endfor %}{% endif %}{% if include.term["equivalent property"] %}<span lang="en">Equivalent Property</span>

{% for equivalent in include.term["equivalent property"] %}: {% include property.markdown term=equivalent %}

{% endfor %}{% endif %}{% if include.term["complement of"] %}<span lang="en">Complement Of</span>

{% for complement_of in include.term["complement of"] %}: {% include class.markdown term=complement_of %}

{% endfor %}{% endif %}{% if include.term["inverse of"] %}<span lang="en">Inverse Of</span>

{% for inverse_of in include.term["inverse of"] %}: {% include property.markdown term=inverse_of %}

{% endfor %}{% endif %}{% if include.term["subclass of"] %}<span lang="en">Subclass Of</span>

{% for subclass_of in include.term["subclass of"] %}: {% include class.markdown term=subclass_of %}

{% endfor %}{% endif %}{% if include.term["superclass of"] %}<span lang="en">Superclass Of</span>

{% for superclass_of in include.term["superclass of"] %}: {% include class.markdown term=superclass_of %}

{% endfor %}{% endif %}{% if include.term["has key"] %}<span lang="en">Target For Key</span>

{% for chain in include.term["has key"] %}: {% for property in chain["@list"] %}{% include property.markdown term=property %}{% unless forloop.last %}, {% endunless %}{% endfor %}

{% endfor %}{% endif %}{% if include.term["subproperty of"] or include.term["property chain axiom"] %}<span lang="en">Subproperty Of</span>

{% if include.term["subproperty of"] %}{% for subproperty_of in include.term["subproperty of"] %}: {% include property.markdown term=subproperty_of %}

{% endfor %}{% endif %}{% if include.term["property chain axiom"] %}{% for chain in include.term["property chain axiom"] %}: {% for property in chain["@list"] %}{% include property.markdown term=property %}{% unless forloop.last %} o {% endunless %}{% endfor %}

{% endfor %}{% endif %}{% endif %}{% if include.term.domain %}<span lang="en">Domain</span>

{% for domain in include.term.domain %}: {% if include.term["@type"] contains "owl:DatatypeProperty" %}{% include datatype.markdown term=domain %}{% else %}{% include class.markdown term=domain %}{% endif %}

{% endfor %}{% endif %}{% if include.term.range %}<span lang="en">Range</span>

{% for range in include.term.range %}: {% if include.term["@type"] contains "owl:DatatypeProperty" %}{% include datatype.markdown term=range %}{% else %}{% include class.markdown term=range %}{% endif %}

{% endfor %}{% endif %}{% if include.term["disjoint with"] %}<span lang="en">Disjoint With</span>

{% for disjoint_with in include.term["disjoint with"] %}: {% include class.markdown term=disjoint_with %}

{% endfor %}{% endif %}{% if include.term["property disjoint with"] %}<span lang="en">Property Disjoint With</span>

{% for disjoint_with in include.term["property disjoint with"] %}: {% include property.markdown term=disjoint_with %}

{% endfor %}{% endif %}{% if include.term["@type"] contains "owl:FunctionalProperty" %}
<span lang="en">Functional?</span>

: <span lang="en">yes</span>

{% endif %}{% if include.term["@type"] contains "owl:InverseFunctionalProperty" %}
<span lang="en">Inverse Functional?</span>

: <span lang="en">yes</span>

{% endif %}{% if include.term["@type"] contains "owl:ReflexiveProperty" %}
<span lang="en">Reflexive?</span>

: <span lang="en">yes</span>

{% endif %}{% if include.term["@type"] contains "owl:IrreflexiveProperty" %}
<span lang="en">Irreflexive?</span>

: <span lang="en">yes</span>

{% endif %}{% if include.term["@type"] contains "owl:SymmetricProperty" %}
<span lang="en">Symmetric?</span>

: <span lang="en">yes</span>

{% endif %}{% if include.term["@type"] contains "owl:AsymmetricProperty" %}
<span lang="en">Asymmetric?</span>

: <span lang="en">yes</span>

{% endif %}{% if include.term["@type"] contains "owl:TransitiveProperty" %}
<span lang="en">Transitive?</span>

: <span lang="en">yes</span>

{% endif %}{% if include.term["is defined by"] %}<span lang="en">Is Defined By</span>

{% for is_defined_by in include.term["is defined by"] %}: [`{{ is_defined_by }}`]({{ is_defined_by }})

{% endfor %}{% endif %}{% if include.term["see also"] %}<span lang="en">See Also</span>

{% for see_also in include.term["see also"] %}: [`{{ see_also }}`][{{ see_also }}]

{% endfor %}{% endif %}{% if include.term["term status"] %}
<span lang="en">Term Status</span>

: <b lang="en">{{ include.term["term status"] }}</b>

{% endif %}
{% for comment in include.term.comment %}<div lang="{{ comment.language | default: "en" }}" markdown="block">
{{ comment.markdown | default: comment }}
</div>
{% endfor %}</section>
