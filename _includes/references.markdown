[1024]: <https://ns.1024.gdn/> "The 1024 Namespace"

[1024/About]: <https://ns.1024.gdn/About/> "The About Vocabulary"
[about:]: <https://ns.1024.gdn/About/#>

[1024/BNS]: <https://ns.1024.gdn/BNS/> "The B·N·S Vocabulary"
[bns:]: <https://ns.1024.gdn/BNS/#>
[bns:Arc]: <https://ns.1024.gdn/BNS/#Arc>
[bns:Book]: <https://ns.1024.gdn/BNS/#Book>
[bns:Branch]: <https://ns.1024.gdn/BNS/#Branch>
[bns:Chapter]: <https://ns.1024.gdn/BNS/#Chapter>
[bns:ComplementaryResource]: <https://ns.1024.gdn/BNS/#ComplementaryResource>
[bns:Concept]: <https://ns.1024.gdn/BNS/#Concept>
[bns:ContentResource]: <https://ns.1024.gdn/BNS/#ContentResource>
[bns:Corpus]: <https://ns.1024.gdn/BNS/#Corpus>
[bns:Cover]: <https://ns.1024.gdn/BNS/#Cover>
[bns:Description]: <https://ns.1024.gdn/BNS/#Description>
[bns:DigitalManifestation]: <https://ns.1024.gdn/BNS/#DigitalManifestation>
[bns:Division]: <https://ns.1024.gdn/BNS/#Division>
[bns:Draft]: <https://ns.1024.gdn/BNS/#Draft>
[bns:File]: <https://ns.1024.gdn/BNS/#File>
[bns:FileSequence]: <https://ns.1024.gdn/BNS/#FileSequence>
[bns:LeafBranch]: <https://ns.1024.gdn/BNS/#LeafBranch>
[bns:Licence]: <https://ns.1024.gdn/BNS/#Licence>
[bns:Note]: <https://ns.1024.gdn/BNS/#Note>
[bns:Project]: <https://ns.1024.gdn/BNS/#Project>
[bns:Pseud]: <https://ns.1024.gdn/BNS/#Pseud>
[bns:Revision]: <https://ns.1024.gdn/BNS/#Revision>
[bns:Section]: <https://ns.1024.gdn/BNS/#Section>
[bns:Side]: <https://ns.1024.gdn/BNS/#Side>
[bns:Soundtrack]: <https://ns.1024.gdn/BNS/#Soundtrack>
[bns:Track]: <https://ns.1024.gdn/BNS/#Track>
[bns:Verse]: <https://ns.1024.gdn/BNS/#Verse>
[bns:Version]: <https://ns.1024.gdn/BNS/#Version>
[bns:Volume]: <https://ns.1024.gdn/BNS/#Volume>
[bns:complements]: <https://ns.1024.gdn/BNS/#complements>
[bns:contents]: <https://ns.1024.gdn/BNS/#contents>
[bns:dateTag]: <https://ns.1024.gdn/BNS/#dateTag>
[bns:fullTitle]: <https://ns.1024.gdn/BNS/#fullTitle>
[bns:hasCover]: <https://ns.1024.gdn/BNS/#hasCover>
[bns:hasCorpus]: <https://ns.1024.gdn/BNS/#hasCorpus>
[bns:hasDigitalManifestation]: <https://ns.1024.gdn/BNS/#hasDigitalManifestation>
[bns:hasDescription]: <https://ns.1024.gdn/BNS/#hasDescription>
[bns:hasLicence]: <https://ns.1024.gdn/BNS/#hasLicence>
[bns:hasNote]: <https://ns.1024.gdn/BNS/#hasNote>
[bns:hasPCDMFileOrDigitalManifestation]: <https://ns.1024.gdn/BNS/#hasPCDMFileOrDigitalManifestation>
[bns:hasProject]: <https://ns.1024.gdn/BNS/#hasProject>
[bns:hasPseud]: <https://ns.1024.gdn/BNS/#hasPseud>
[bns:hasSoundtrack]: <https://ns.1024.gdn/BNS/#hasSoundtrack>
[bns:hasTrack]: <https://ns.1024.gdn/BNS/#hasTrack>
[bns:identifier]: <https://ns.1024.gdn/BNS/#identifier>
[bns:in]: <https://ns.1024.gdn/BNS/#in>
[bns:index]: <https://ns.1024.gdn/BNS/#index>
[bns:includes]: <https://ns.1024.gdn/BNS/#includes>
[bns:isComplementedBy]: <https://ns.1024.gdn/BNS/#isComplementedBy>
[bns:isCorpusOf]: <https://ns.1024.gdn/BNS/#isCorpusOf>
[bns:isCoverOf]: <https://ns.1024.gdn/BNS/#isCoverOf>
[bns:isDigitalManifestationOf]: <https://ns.1024.gdn/BNS/#isDigitalManifestationOf>
[bns:isDescriptionOf]: <https://ns.1024.gdn/BNS/#isDescriptionOf>
[bns:isLicenceOf]: <https://ns.1024.gdn/BNS/#isLicenceOf>
[bns:isNoteOf]: <https://ns.1024.gdn/BNS/#isNoteOf>
[bns:isProjectIn]: <https://ns.1024.gdn/BNS/#isProjectIn>
[bns:isPseudOf]: <https://ns.1024.gdn/BNS/#isPseudOf>
[bns:isSoundtrackOf]: <https://ns.1024.gdn/BNS/#isSoundtrackOf>
[bns:isTrackIn]: <https://ns.1024.gdn/BNS/#isTrackIn>
[bns:mediaType]: <https://ns.1024.gdn/BNS/#mediaType>
[bns:nameTag]: <https://ns.1024.gdn/BNS/#nameTag>
[bns:shortTitle]: <https://ns.1024.gdn/BNS/#shortTitle>
[bns:textLabel]: <https://ns.1024.gdn/BNS/#textLabel>
[bns:transitivelyIncludes]: <https://ns.1024.gdn/BNS/#transitivelyIncludes>

[1024/KDL]: <https://ns.1024.gdn/KDL/> "The K·D·L Vocabulary"
[kdl:]: <https://ns.1024.gdn/KDL/#>
[kdl:Argument]: <https://ns.1024.gdn/KDL/#Argument>
[kdl:Document]: <https://ns.1024.gdn/KDL/#Document>
[kdl:Identifier]: <https://ns.1024.gdn/KDL/#Identifier>
[kdl:NamespacedIdentifier]: <https://ns.1024.gdn/KDL/#NamespacedIdentifier>
[kdl:Node]: <https://ns.1024.gdn/KDL/#Node>
[kdl:NullParameter]: <https://ns.1024.gdn/KDL/#NullParameter>
[kdl:Parameter]: <https://ns.1024.gdn/KDL/#Parameter>
[kdl:Property]: <https://ns.1024.gdn/KDL/#Property>
[kdl:TypeAnnotation]: <https://ns.1024.gdn/KDL/#TypeAnnotation>
[kdl:expandedForm]: <https://ns.1024.gdn/KDL/#expandedForm>
[kdl:hasChildren]: <https://ns.1024.gdn/KDL/#hasChildren>
[kdl:hasName]: <https://ns.1024.gdn/KDL/#hasName>
[kdl:hasNodes]: <https://ns.1024.gdn/KDL/#hasNodes>
[kdl:hasParameters]: <https://ns.1024.gdn/KDL/#hasParameters>
[kdl:hasTypeAnnotation]: <https://ns.1024.gdn/KDL/#hasTypeAnnotation>
[kdl:isNull]: <https://ns.1024.gdn/KDL/#isNull>
[kdl:localPart]: <https://ns.1024.gdn/KDL/#localPart>
[kdl:namespace]: <https://ns.1024.gdn/KDL/#namespace>
[kdl:nodesContainedIn]: <https://ns.1024.gdn/KDL/#nodesContainedIn>
[kdl:parametersContainedIn]: <https://ns.1024.gdn/KDL/#parametersContainedIn>
[kdl:prefix]: <https://ns.1024.gdn/KDL/#prefix>

[1024/Roleplaying]: <https://ns.1024.gdn/Roleplaying/> "The Roleplaying Vocabulary"
[rp:]: <https://ns.1024.gdn/Roleplaying/#>

[1024/Sync]: <https://ns.1024.gdn/Sync/> "The Sync Vocabulary"
[sync:]: <https://ns.1024.gdn/Sync/#>
[sync:SyncActivity]: <https://ns.1024.gdn/Sync/#SyncActivity>
[sync:states]: <https://ns.1024.gdn/Sync/#states>
[sync:unstates]: <https://ns.1024.gdn/Sync/#unstates>

[1024/Tagging]: <https://ns.1024.gdn/Tagging/> "The Tagging Vocabulary"
[tagging:]: <https://ns.1024.gdn/Tagging/#>
[tagging:CanonTag]: <https://ns.1024.gdn/Tagging/#CanonTag>
[tagging:CharacterTag]: <https://ns.1024.gdn/Tagging/#CharacterTag>
[tagging:ConceptualTag]: <https://ns.1024.gdn/Tagging/#ConceptualTag>
[tagging:EntityTag]: <https://ns.1024.gdn/Tagging/#EntityTag>
[tagging:FamilialRelationshipTag]: <https://ns.1024.gdn/Tagging/#FamilialRelationshipTag>
[tagging:FriendshipTag]: <https://ns.1024.gdn/Tagging/#FriendshipTag>
[tagging:GenreTag]: <https://ns.1024.gdn/Tagging/#GenreTag>
[tagging:InanimateEntityTag]: <https://ns.1024.gdn/Tagging/#InanimateEntityTag>
[tagging:LocationTag]: <https://ns.1024.gdn/Tagging/#LocationTag>
[tagging:OrderedTagLabelCollection]: <https://ns.1024.gdn/Tagging/#OrderedTagLabelCollection>
[tagging:RelationshipTag]: <https://ns.1024.gdn/Tagging/#RelationshipTag>
[tagging:Refinement]: <https://ns.1024.gdn/Tagging/#Refinement>
[tagging:RivalryTag]: <https://ns.1024.gdn/Tagging/#RivalryTag>
[tagging:RomanticRelationshipTag]: <https://ns.1024.gdn/Tagging/#RomanticRelationshipTag>
[tagging:SettingTag]: <https://ns.1024.gdn/Tagging/#SettingTag>
[tagging:SexualRelationshipTag]: <https://ns.1024.gdn/Tagging/#SexualRelationshipTag>
[tagging:Tag]: <https://ns.1024.gdn/Tagging/#Tag>
[tagging:TagActivity]: <https://ns.1024.gdn/Tagging/#TagActivity>
[tagging:TagLabel]: <https://ns.1024.gdn/Tagging/#TagLabel>
[tagging:TagLabelCollection]: <https://ns.1024.gdn/Tagging/#TagLabelCollection>
[tagging:TagSet]: <https://ns.1024.gdn/Tagging/#TagSet>
[tagging:TagSystem]: <https://ns.1024.gdn/Tagging/#TagSystem>
[tagging:TimePeriodTag]: <https://ns.1024.gdn/Tagging/#TimePeriodTag>
[tagging:UniverseTag]: <https://ns.1024.gdn/Tagging/#UniverseTag>
[tagging:containsTagLabel]: <https://ns.1024.gdn/Tagging/#containsTagLabel>
[tagging:hasLabel]: <https://ns.1024.gdn/Tagging/#hasLabel>
[tagging:expressesRefinement]: <https://ns.1024.gdn/Tagging/#expressesRefinement>
[tagging:hasPrimaryTagLabelCollection]: <https://ns.1024.gdn/Tagging/#hasPrimaryTagLabelCollection>
[tagging:hasTagLabelCollection]: <https://ns.1024.gdn/Tagging/#hasTagLabelCollection>
[tagging:hasTagLabelList]: <https://ns.1024.gdn/Tagging/#hasTagLabelList>
[tagging:inCanon]: <https://ns.1024.gdn/Tagging/#inCanon>
[tagging:inSystem]: <https://ns.1024.gdn/Tagging/#inSystem>
[tagging:involves]: <https://ns.1024.gdn/Tagging/#involves>
[tagging:labels]: <https://ns.1024.gdn/Tagging/#labels>
[tagging:onTag]: <https://ns.1024.gdn/Tagging/#onTag>

[ActivityStreams]: <https://www.w3.org/TR/activitystreams-core> "Activity Streams 2.0"
[Activity]: <https://www.w3.org/TR/activitystreams-vocabulary>
[as:]: <https://www.w3.org/ns/activitystreams#>
[as:Accept]: <https://www.w3.org/ns/activitystreams#Accept>
[as:Activity]: <https://www.w3.org/ns/activitystreams#Activity>
[as:Collection]: <https://www.w3.org/ns/activitystreams#Collection>
[as:CollectionPage]: <https://www.w3.org/ns/activitystreams#CollectionPage>
[as:Link]: <https://www.w3.org/ns/activitystreams#Link>
[as:Object]: <https://www.w3.org/ns/activitystreams#Object>
[as:Offer]: <https://www.w3.org/ns/activitystreams#Offer>
[as:OrderedCollection]: <https://www.w3.org/ns/activitystreams#OrderedCollection>
[as:OrderedCollectionPage]: <https://www.w3.org/ns/activitystreams#OrderedCollectionPage>
[as:Reject]: <https://www.w3.org/ns/activitystreams#Reject>
[as:Relationship]: <https://www.w3.org/ns/activitystreams#Relationship>
[as:Tombstone]: <https://www.w3.org/ns/activitystreams#Reject>
[as:actor]: <https://www.w3.org/ns/activitystreams#actor>
[as:attributedTo]: <https://www.w3.org/ns/activitystreams#attributedTo>
[as:endTime]: <https://www.w3.org/ns/activitystreams#endTime>
[as:context]: <https://www.w3.org/ns/activitystreams#context>
[as:first]: <https://www.w3.org/ns/activitystreams#first>
[as:items]: <https://www.w3.org/ns/activitystreams#items>
[as:last]: <https://www.w3.org/ns/activitystreams#last>
[as:location]: <https://www.w3.org/ns/activitystreams#location>
[as:next]: <https://www.w3.org/ns/activitystreams#next>
[as:object]: <https://www.w3.org/ns/activitystreams#object>
[as:origin]: <https://www.w3.org/ns/activitystreams#origin>
[as:partOf]: <https://www.w3.org/ns/activitystreams#partOf>
[as:published]: <https://www.w3.org/ns/activitystreams#published>
[as:prev]: <https://www.w3.org/ns/activitystreams#prev>
[as:target]: <https://www.w3.org/ns/activitystreams#target>
[as:updated]: <https://www.w3.org/ns/activitystreams#updated>

[Changeset]: <https://vocab.org/changeset/schema>

[DCMI]: <https://www.dublincore.org/specifications/dublin-core/dcmi-terms/> "DCMI Metadata Terms"
[dc11:]: <http://purl.org/dc/elements/1.1/>
[dc11:format]: <http://purl.org/dc/elements/1.1/format>
[dcterms:]: <http://purl.org/dc/terms/>
[dcterms:Agent]: <http://purl.org/dc/terms/Agent>
[dcterms:RightsStatement]: <http://purl.org/dc/terms/RightsStatement>
[dcterms:LicenseDocument]: <http://purl.org/dc/terms/LicenseDocument>
[dcterms:alternative]: <http://purl.org/dc/terms/alternative>
[dcterms:created]: <http://purl.org/dc/terms/created>
[dcterms:date]: <http://purl.org/dc/terms/date>
[dcterms:hasPart]: <http://purl.org/dc/terms/hasPart>
[dcterms:description]: <http://purl.org/dc/terms/description>
[dcterms:identifier]: <http://purl.org/dc/terms/identifier>
[dcterms:isPartOf]: <http://purl.org/dc/terms/isPartOf>
[dcterms:isReferencedBy]: <http://purl.org/dc/terms/isReferencedBy>
[dcterms:license]: <http://purl.org/dc/terms/license>
[dcterms:modified]: <http://purl.org/dc/terms/modified>
[dcterms:references]: <http://purl.org/dc/terms/references>
[dcterms:relation]: <http://purl.org/dc/terms/relation>
[dcterms:rights]: <http://purl.org/dc/terms/rights>
[dcterms:source]: <http://purl.org/dc/terms/source>
[dcterms:subject]: <http://purl.org/dc/terms/subject>
[dcterms:title]: <http://purl.org/dc/terms/title>
[dcmitype:]: <http://purl.org/dc/dcmitype/>
[dcmitype:Collection]: <http://purl.org/dc/dcmitype/Collection>
[dcmitype:Dataset]: <http://purl.org/dc/dcmitype/Dataset>
[dcmitype:Event]: <http://purl.org/dc/dcmitype/Event>
[dcmitype:Image]: <http://purl.org/dc/dcmitype/Image>
[dcmitype:InteractiveResource]: <http://purl.org/dc/dcmitype/InteractiveResource>
[dcmitype:MovingImage]: <http://purl.org/dc/dcmitype/MovingImage>
[dcmitype:PhysicalObject]: <http://purl.org/dc/dcmitype/PhysicalObject>
[dcmitype:Service]: <http://purl.org/dc/dcmitype/Service>
[dcmitype:Software]: <http://purl.org/dc/dcmitype/Software>
[dcmitype:Sound]: <http://purl.org/dc/dcmitype/Sound>
[dcmitype:StillImage]: <http://purl.org/dc/dcmitype/StillImage>
[dcmitype:Text]: <http://purl.org/dc/dcmitype/Text>

[FOAF]: <http://xmlns.com/foaf/spec/> "FOAF Vocabulary Specification"
[foaf:]: <http://xmlns.com/foaf/0.1/>
[foaf:Agent]: <http://xmlns.com/foaf/0.1/Agent>
[foaf:Document]: <http://xmlns.com/foaf/0.1/Document>
[foaf:Image]: <http://xmlns.com/foaf/0.1/Image>
[foaf:Project]: <http://xmlns.com/foaf/0.1/Project>

[JSON-LD]: <https://www.w3.org/TR/json-ld/> "JSON-LD"

[OA]: <https://www.w3.org/TR/annotation-vocab/> "Web Annotation Vocabulary"
[oa:]: <http://www.w3.org/ns/oa#>
[oa:Motivation]: <http://www.w3.org/ns/oa#Motivation>
[oa:TextualBody]: <http://www.w3.org/ns/oa#TextualBody>
[oa:describing]: <http://www.w3.org/ns/oa#describing>

[OAI-ORE]: <http://openarchives.org/ore/toc> "ORE Specifications and User Guides - Table of Contents"
[OAI-ORE/Vocabulary]: <https://www.openarchives.org/ore/vocabulary> "ORE Specification - Vocabulary"
[ore:]: <http://www.openarchives.org/ore/terms/>
[ore:Aggregation]: <http://www.openarchives.org/ore/terms/Aggregation>
[ore:AggregatedResource]: <http://www.openarchives.org/ore/terms/AggregatedResource>
[ore:Proxy]: <http://www.openarchives.org/ore/terms/Proxy>
[ore:aggregates]: <http://www.openarchives.org/ore/terms/aggregates>
[ore:isAggregatedBy]: <http://www.openarchives.org/ore/terms/isAggregatedBy>
[ore:proxyFor]: <http://www.openarchives.org/ore/terms/proxyFor>
[ore:proxyIn]: <http://www.openarchives.org/ore/terms/proxyIn>

[OWL]: <https://www.w3.org/TR/owl-overview/> "OWL 2 Web Ontology Language: Document Overview"
[owl:]: <http://www.w3.org/2002/07/owl#>
[owl:Thing]: <http://www.w3.org/2002/07/owl#Thing>

[PCDM]: <https://pcdm.org> "PCDM Ontologies"
[pcdm:]: <http://pcdm.org/models#>
[pcdm:Collection]: <http://pcdm.org/models#Collection>
[pcdm:File]: <http://pcdm.org/models#File>
[pcdm:Object]: <http://pcdm.org/models#Object>
[pcdm:fileOf]: <http://pcdm.org/models#fileOf>
[pcdm:hasFile]: <http://pcdm.org/models#hasFile>
[pcdm:hasMember]: <http://pcdm.org/models#hasMember>
[pcdm:hasRelatedObject]: <http://pcdm.org/models#hasRelatedObject>
[pcdm:memberOf]: <http://pcdm.org/models#memberOf>
[pcdm:relatedObjectOf]: <http://pcdm.org/models#relatedObjectOf>

[PROV-O]: <https://www.w3.org/TR/prov-o> "PROV-O: The PROV Ontology"
[prov:]: <http://www.w3.org/ns/prov#>
[prov:Activity]: <http://www.w3.org/ns/prov#Activity>
[prov:Agent]: <http://www.w3.org/ns/prov#Agent>
[prov:Collection]: <http://www.w3.org/ns/prov#Collection>
[prov:Entity]: <http://www.w3.org/ns/prov#Entity>
[prov:generated]: <http://www.w3.org/ns/prov#generated>
[prov:hadMember]: <http://www.w3.org/ns/prov#hadMember>
[prov:influenced]: <http://www.w3.org/ns/prov#influenced>
[prov:invalidated]: <http://www.w3.org/ns/prov#invalidated>
[prov:wasInfluencedBy]: <http://www.w3.org/ns/prov#wasInfluencedBy>

[rdf:]: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
[rdf:Bag]: <http://www.w3.org/1999/02/22-rdf-syntax-ns#Bag>
[rdf:List]: <http://www.w3.org/1999/02/22-rdf-syntax-ns#List>
[rdf:PlainLiteral]: <http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral>
[rdf:XMLLiteral]: <http://www.w3.org/1999/02/22-rdf-syntax-ns#XMLLiteral>
[rdf:Seq]: <http://www.w3.org/1999/02/22-rdf-syntax-ns#Seq>
[rdf:Statement]: <http://www.w3.org/1999/02/22-rdf-syntax-ns#Statement>
[rdf:first]: <http://www.w3.org/1999/02/22-rdf-syntax-ns#first>
[rdf:nil]: <http://www.w3.org/1999/02/22-rdf-syntax-ns#nil>
[rdf:object]: <http://www.w3.org/1999/02/22-rdf-syntax-ns#object>
[rdf:predicate]: <http://www.w3.org/1999/02/22-rdf-syntax-ns#predicate>
[rdf:rest]: <http://www.w3.org/1999/02/22-rdf-syntax-ns#rest>
[rdf:subject]: <http://www.w3.org/1999/02/22-rdf-syntax-ns#subject>
[rdf:value]: <http://www.w3.org/1999/02/22-rdf-syntax-ns#value>

[rdfs:]: <http://www.w3.org/2000/01/rdf-schema#>
[rdfs:Container]: <http://www.w3.org/2000/01/rdf-schema#Container>
[rdfs:Literal]: <http://www.w3.org/2000/01/rdf-schema#Literal>
[rdfs:comment]: <http://www.w3.org/2000/01/rdf-schema#comment>
[rdfs:label]: <http://www.w3.org/2000/01/rdf-schema#label>
[rdfs:member]: <http://www.w3.org/2000/01/rdf-schema#member>
[rdfs:seeAlso]: <http://www.w3.org/2000/01/rdf-schema#seeAlso>

[Relation]: <http://www.iana.org/assignments/link-relations/>
[rel:]: <http://www.iana.org/assignments/relation/>
[rel:first]: <http://www.iana.org/assignments/relation/first>
[rel:last]: <http://www.iana.org/assignments/relation/last>
[rel:next]: <http://www.iana.org/assignments/relation/next>
[rel:prev]: <http://www.iana.org/assignments/relation/prev>

[SKOS]: <http://www.w3.org/TR/skos-reference> "SKOS Simple Knowledge Organization System Reference"
[skos:]: <http://www.w3.org/2004/02/skos/core#>
[skos:Collection]: <http://www.w3.org/2004/02/skos/core#Collection>
[skos:Concept]: <http://www.w3.org/2004/02/skos/core#Concept>
[skos:ConceptScheme]: <http://www.w3.org/2004/02/skos/core#ConceptScheme>
[skos:altLabel]: <http://www.w3.org/2004/02/skos/core#altLabel>
[skos:broader]: <http://www.w3.org/2004/02/skos/core#broader>
[skos:broaderTransitive]: <http://www.w3.org/2004/02/skos/core#broaderTransitive>
[skos:closeMatch]: <http://www.w3.org/2004/02/skos/core#closeMatch>
[skos:hiddenLabel]: <http://www.w3.org/2004/02/skos/core#hiddenLabel>
[skos:inScheme]: <http://www.w3.org/2004/02/skos/core#inScheme>
[skos:mappingRelation]: <http://www.w3.org/2004/02/skos/core#mappingRelation>
[skos:member]: <http://www.w3.org/2004/02/skos/core#member>
[skos:narrower]: <http://www.w3.org/2004/02/skos/core#narrower>
[skos:narrowerTransitive]: <http://www.w3.org/2004/02/skos/core#narrowerTransitive>
[skos:prefLabel]: <http://www.w3.org/2004/02/skos/core#prefLabel>
[skos:semanticRelation]: <http://www.w3.org/2004/02/skos/core#semanticRelation>

[SKOS-XL]: <http://www.w3.org/TR/skos-reference#xl> "SKOS eXtension for Labels (SKOS-XL)"
[skosxl:]: <http://www.w3.org/2008/05/skos-xl#>
[skosxl:Label]: <http://www.w3.org/2008/05/skos-xl#Label>
[skosxl:literalForm]: <http://www.w3.org/2008/05/skos-xl#literalForm>
[skosxl:altLabel]: <http://www.w3.org/2008/05/skos-xl#altLabel>
[skosxl:hiddenLabel]: <http://www.w3.org/2008/05/skos-xl#hiddenLabel>
[skosxl:prefLabel]: <http://www.w3.org/2008/05/skos-xl#prefLabel>

[VS]: <https://www.w3.org/2003/06/sw-vocab-status/note> "Term-centric Semantic Web Vocabulary Annotations"

[xsd:]: <http://www.w3.org/2001/XMLSchema#>
[xsd:NCName]: <http://www.w3.org/2001/XMLSchema#NCName>
[xsd:boolean]: <http://www.w3.org/2001/XMLSchema#boolean>
[xsd:date]: <http://www.w3.org/2001/XMLSchema#date>
[xsd:dateTime]: <http://www.w3.org/2001/XMLSchema#dateTime>
[xsd:decimal]: <http://www.w3.org/2001/XMLSchema#decimal>
[xsd:gYearMonth]: <http://www.w3.org/2001/XMLSchema#gYearMonth>
[xsd:integer]: <http://www.w3.org/2001/XMLSchema#integer>
[xsd:nonNegativeInteger]: <http://www.w3.org/2001/XMLSchema#nonNegativeInteger>
[xsd:pattern]: <http://www.w3.org/2001/XMLSchema#pattern>
[xsd:positiveInteger]: <http://www.w3.org/2001/XMLSchema#positiveInteger>
[xsd:string]: <http://www.w3.org/2001/XMLSchema#string>

[AO3]: <https://archiveofourown.org> "Archive of Our Own"
[IIIF-Discovery]: <https://iiif.io/api/discovery/1.0/> "IIIF Change Discovery API 1.0"
[KDL]: <https://kdl.dev> "The KDL Document Language"
[Media-Types]: <https://www.iana.org/assignments/media-types/> "Media Types"
[Protégé]: <https://protege.stanford.edu> "protégé"
[RFC4151]: <https://datatracker.ietf.org/doc/html/rfc4151> "The 'tag' URI Scheme"
[RFC7033]: <https://datatracker.ietf.org/doc/html/rfc7033> "WebFinger"
[Semver]: <https://semver.org> "Semantic Versioning"
[WRMGBase32]: <https://www.crockford.com/base32.html> "Base 32"
[kibigo!]: <https://go.KIBI.family/About/#me> "kibigo!"
[marrus-sh/Corpora]: <https://github.com/marrus-sh/Corpora> "Branching Notational System Corpora"
