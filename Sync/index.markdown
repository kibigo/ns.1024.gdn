---
title: The Sync Vocabulary
links:
- rel: meta
  type: application/ld+json
  href: "owl.jsonld"
---

## Abstract {#abstract}

The <cite>Sync</cite> vocabulary provides useful terms for syncing
descriptions of R·D·F resources between servers.

{% include toc.markdown %}

{% include references.markdown %}

## <span class="numero">1.</span> Introduction {#introduction}

### <span class="numero">1.1</span> Purpose and Scope
{: id="introduction.purpose"}

The aim of this vocabulary is to provide a general mechanism for
  publishing a series of fairly simple changes to the R·D·F
  descriptions of one or more objects, as well as enabling the
  discovery of new objects via the same mechanism.
The intent is to create a stream of [Activities](#as:Activity) which
  can be replayed to reconstruct an equivalent graph on a different
  service.
It has the following features :&#x2060;—

 +  Ordered serialization of timestamped events through [Ordered
      Collection Pages](#as:OrderedCollectionPage) of [Sync
      Activities](#SyncActivity) with [end times](#as:endTime),
      enabling consumers to only read back events since the last time
      they checked.

 +  Easy identification of the resource descriptions being changed by
      requiring each Sync Activity to contain
      [Statements](#rdf:Statement) with only one
      [subject](#rdf:subject), implied by the [object](#as:object) of
      the Activity.

 +  Recommendations for associating Activities with
      [labels](#rdfs:label) and [comments](#rdfs:comment).

The namespace for the <cite>Sync</cite> vocabulary is
  [`https://ns.1024.gdn/Sync/#`][sync:].

### <span class="numero">1.2</span> Relationship to Other Vocabularies
{: id="introduction.relationships"}

<cite>Sync</cite> is built upon the framework provided by
  [ActivityStreams][] and the [<cite>Activity</cite>][Activity]
  vocabulary, although broader ActivityStreams support is not expected
  or required.
It follows the general model and approach of [<cite>I·I·I·F Change
  Discovery</cite>][IIIF-Discovery] and similar uses of ActivityStreams
  for change tracking.
It operates on a similar principle to the earlier
  [<cite>Changeset</cite>][Changeset] vocabulary, but does not
  reference its terms.

[<cite>Prov‐O</cite>][PROV-O] is used as a (light) ontological
  dependency; in particular, its [generated](#prov:generated) and
  [invalidated](#prov:invalidated) properties.

In this document, the following prefixes are used to represent the
  following strings :&#x2060;—

| Prefix | Expansion |
| :----: | :-------: |
| `as:` | [`https://www.w3.org/ns/activitystreams#`][as:] |
| `prov:` | [`http://www.w3.org/ns/prov#`][prov:] |
| `rdf:` | [`http://www.w3.org/1999/02/22-rdf-syntax-ns#`][rdf:] |
| `rdfs:` | [`http://www.w3.org/2000/01/rdf-schema#`][rdfs:] |
| `sync:` | [`https://ns.1024.gdn/Sync/#`][sync:] |
| `xsd:` | [`http://www.w3.org/2001/XMLSchema#`][xsd:] |

### <span class="numero">1.3</span> Term Status
{:id="introduction.status"}

Terms in this document are categorized according to one the five
  following <dfn id="dfn.term_status">term statuses</dfn> :&#x2060;—

stable (✅)

: The term has widely‐accepted usage.

testing (🔬)

: The term is currently being used in some applications, but its exact
    meaning has not yet settled.

dependency (📥)

: The term is not currently being used directly, but its definition
    informs the usage of a stable or testing term.
  (This value is not a part of the original [term status][VS]
    definition.)

unstable (🚧)

: The term is not currently being used in a widespread fashion, but
    experiments into its utility are taking place.
  Its usage may change significantly in the future.

archaic (🦖)

: The term was once widely‐used, but such usage is considered outdated
    and is discouraged.

### <span class="numero">1.4</span> Definitions for Computers
{:id="introduction.computers"}

There is an [Owl][OWL] [ontology](./owl.jsonld) available in a
  [J·son‐L·D][JSON-LD] format suitable for viewing with, e·g
  [Protégé][].
The term definitions seen on this page are derived from the Owl
  definitions.

There also exists [a J·son‐L·D context](./context.jsonld) for use when
  referencing this vocabulary’s terms.

Generally, only stable and testing terms will be included in the
  J·son‐L·D context.
If you need to use an unstable term, you will need to define it
  yourself.

### <span class="numero">1.5</span> Using this Vocabulary
{:id="introduction.usage"}

It is recommended that changes to a system be provided via a paginated
  [Ordered Collection](#as:OrderedCollection), pointed to via the
  [outbox](#as:outbox) property.
The items in this collection should all be [Sync
  Activities](#SyncActivity) with the system provided as their
  [context](#as:context).
Per ActivityStreams, at least a [J·son‐L·D][JSON-LD] representation
  should be available, ideally one which is compact under the
  [<cite>Sync</cite> J·son‐L·D context](./context.jsonld) to enable its
  use by non–Linked Data‐aware processors.

Be aware that even compacted J·son‐L·D carries different semantics than
  ordinary J·son—particularly with respect to its interpretation of
  arrays.

{% include ontology.markdown name="Sync" numero="2" %}
