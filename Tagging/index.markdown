---
title: The Tagging Vocabulary
links:
- rel: meta
  type: application/ld+json
  href: "owl.jsonld"
---

## Abstract {#abstract}

The <cite>Tagging</cite> vocabulary provides useful terms for defining
and associating resources with semantic labels known as “tags”.

{% include toc.markdown %}

{% include references.markdown %}

## <span class="numero">1.</span> Introduction {#introduction}

### <span class="numero">1.1</span> Purpose and Scope
{:id="introduction.purpose"}

This vocabulary grows out of a general effort to move various
  publishing efforts, and in particular fannish ones, off of
  centralized platforms in an interoperable manner.
On some platforms, for example [<cite>Archive of Our Own</cite>][AO3],
  such works are currently extensively tagged with labels which are
  then “wrangled” into a conceptual heirarchy.
This is labour‐intensive work, and it is important that *federated*
  fannish repositories be able to share and communicate such efforts
  with each other.
The <cite>Tagging</cite> vocabulary forms the basis of a means for
  doing so.

[Tags](#Tag) have been designed to satisfy the following
  requirements :&#x2060;—

 +  Each Tag may be associated with many [Tag Labels](#TagLabel), but
      only has a single [preferred one](#skosxl:prefLabel).

 +  Tags may be organized into a conceptual heirarchy with some Tags
      being [broader](#skos:broader) or [narrower](#skos:narrower) than
      others.

 +  Each Tag belongs to a single [Tag System](#TagSystem) which is
      responsible for its maintenance, but Tags in different Tag
      Systems can be indicated as [close matches](#skos:closeMatch) for
      each other.

 +  Tags belong to different classes, with some classes having the
      option for other kinds of semantic relation than simple
      broadening or narrowing.

Tags are *not* intended as a catch·all solution for work metadata.
Other kinds of information which does not necessitate these
  requirements—for example, content notices with a single,
  welrecognized label—are probably not appropriate for encoding as
  Tags.

The namespace for the <cite>Tagging</cite> vocabulary is
  [`https://ns.1024.gdn/Tagging/#`][tagging:].

### <span class="numero">1.2</span> Relationship to Other Vocabularies
{:id="introduction.relationships"}

<cite>Tagging</cite> is conceived of as an extension to
  [<cite>S·K·O·S</cite>][SKOS], and makes use of some of its
  properties.
The [<cite>X·L</cite>][SKOS-XL] extenstion of <cite>S·K·O·S</cite> is
  used for label construction.

[<cite>D·C·M·I</cite>][DCMI] is used for basic metadata.

[<cite>Prov‐O</cite>][PROV-O] is an ontological dependency for some
  terms, but familiarity is not required.

The additional terms for discovery are built upon the
  [<cite>Sync</cite>][1024/Sync] vocabulary, which is itself built upon
  the [<cite>Activity</cite>][Activity] vocabulary and
  [ActivityStreams][].

In this document, the following prefixes are used to represent the
  following strings :&#x2060;—

| Prefix | Expansion |
| :----: | :-------: |
| `as:` | [`https://www.w3.org/ns/activitystreams#`][as:] |
| `dcterms:` | [`http://purl.org/dc/terms/`][dcterms:] |
| `prov:` | [`http://www.w3.org/ns/prov#`][prov:] |
| `skos:` | [`http://www.w3.org/2004/02/skos/core#`][skos:] |
| `skosxl:` | [`http://www.w3.org/2008/05/skos-xl#`][skosxl:] |
| `sync:` | [`https://ns.1024.gdn/Sync/#`][sync:] |
| `tagging:` | [`https://ns.1024.gdn/Tagging/#`][tagging:] |
| `rdf:` | [`http://www.w3.org/1999/02/22-rdf-syntax-ns#`][rdf:] |
| `rdfs:` | [`http://www.w3.org/2000/01/rdf-schema#`][rdfs:] |
| `xsd:` | [`http://www.w3.org/2001/XMLSchema#`][xsd:] |

### <span class="numero">1.3</span> Term Status
{:id="introduction.status"}

Terms in this document are categorized according to one the five
  following <dfn id="dfn.term_status">term statuses</dfn> :&#x2060;—

stable (✅)

: The term has widely‐accepted usage.

testing (🔬)

: The term is currently being used in some applications, but its exact
    meaning has not yet settled.

dependency (📥)

: The term is not currently being used directly, but its definition
    informs the usage of a stable or testing term.
  (This value is not a part of the original [term status][VS]
    definition.)

unstable (🚧)

: The term is not currently being used in a widespread fashion, but
    experiments into its utility are taking place.
  Its usage may change significantly in the future.

archaic (🦖)

: The term was once widely‐used, but such usage is considered outdated
    and is discouraged.

### <span class="numero">1.4</span> Definitions for Computers
{:id="introduction.computers"}

There is an [Owl][OWL] [ontology for the main vocabulary](./owl.jsonld)
  and a separate [ontology for the discovery
  vocabulary](./discovery.owl.jsonld) (which imports the former).
Both are provided in a [J·son‐L·D][JSON-LD] format suitable for viewing
  with, e·g, [Protégé][]. The term definitions seen on this page are
  derived from the Owl definitions.

There also exist two J·son‐L·D contexts :&#x2060;— [a J·son‐L·D context for
  the main vocabulary](./context.jsonld) and [a J·son‐L·D context for
  the discovery vocabulary](./discovery.context.jsonld) (which, again,
  makes reference to the former).

Generally, only stable and testing terms will be included in the
  J·son‐L·D contexts for this vocabulary.
If you need to use an unstable term, you will need to define it
  yourself.

### <span class="numero">1.5</span> Using this Vocabulary
{:id="introduction.usage"}

In most cases, at least a [J·son‐L·D][JSON-LD] representation of any
  [Tag](#Tag) or [Tag System](#TagSystem) should be attainable from its
  I·R·I.
This representation should be compact under the [<cite>Tagging</cite>
  J·son‐L·D context](./context.jsonld) to enable its use by non–Linked
  Data‐aware processors.
There are recommendations below which can further enable services to
  discover changes to Tags made over time.

Be aware that even compacted J·son‐L·D carries different semantics than
  ordinary J·son—particularly with respect to its interpretation of
  arrays.

## <span class="numero">2</span> Overview of Concepts {#overview}

### <span class="numero">2.1</span> Tags
{:id="overview.tags"}

A [Tag](#Tag) is an [<cite>S·K·O·S</cite>][SKOS]
  [Concept](#skos:Concept) which [is in](#inSystem) a [Tag
  System](#TagSystem), [has a preferred label](#prefLabel) (and
  possibly others), and may be related to other Tags.
The precise meaning beyond these constraints is flexible.
Generally, the intent of Tags is to aid in the discovery or filtering
  of things by associating them with recognized terms.

Things are typically not associated with Tags directly; instead, things
  are associated with [Tag Labels](#TagLabel) which [label](#labels)
  Tags (see below).
This layer of indirection allows things to provide the most appropriate
  contextual label for their circumstance while still being associated
  with a Tag of defined meaning.

#### <span class="numero">2.1.1</span> Tag I·R·I’s
{:id="overview.tags.identifiers"}

Tags need stable I·R·I’s in order to be referenced and looked up.
The recommendation is that the I·R·I take the following form :&#x2060;—

    https://⟨authority⟩/tag:⟨authority⟩,⟨date⟩:⟨identifier⟩

—: where `⟨authority⟩` is the domain name for the site, `⟨date⟩` is
  some stable date in which the site was controlled by its current
  owner(s) (presumably the people publishing the tags), and
  `⟨identifier⟩` is the local (not necessarily global) identifier of
  the tag on that site.
The section of the I·R·I beginning with `tag:` and continuing through
  to the end forms an embedded [Tag U·R·I][RFC4151] (no relation) which
  can be used to help identify the tag in the case of mirroring or
  migration.
The Tag U·R·I might be separated from the authority by a path if
  desired (it doesn’t need to be top‐level).

The local identifier for the tag can be anything which conforms to
  the `specific` rule in the Tag U·R·I syntax, but it is strongly
  recommended that it be a short, opaque identifier which is suitable
  for humans.
Assigning each tag a random number, encoded in [W·R·M·G (Crockford)
  base·32][WRMGBase32], is a good option.
Do not derive tag identifiers from the labels of tags, as these may
  change in the future (and the identifier must not).

The above I·R·I is a bit cumbersome and unweildy for humans (it repeats
  the authority twice!), so a service might create a more readable
  alias (for example, `https://⟨authority⟩/⟨identifier⟩`).
However, this alias should never be used when communicating with
  computers, as the identifier is not globally‐unique on its own and
  can never be migrated to other hosts.

Services should always try to fetch Tags from the domain listed in the
  Tag U·R·I if possible.
If they encounter a Tag where the authority of the I·R·I as a whole
  doesn’t match that in the Tag U·R·I, they might attempt a
  [Webfinger][RFC7033] request to the latter in an attempt to fetch the
  canonical version of the Tag.
Otherwise, they should view the Tag with suspicion.
Nevertheless, allowing for mirroring of tags in this manner is
  necessary to accommodate their persistence beyond site and service
  shutdowns.

#### <span class="numero">2.1.2</span> Relationships between Tags
{:id="overview.tags.relationships"}

Any Tag may be organized into a conceptual hierarchy by marking it as
  [broader](#skos:broader) or [narrower](#skos:narrower) than another
  tag.
These relationships may extend across different kinds of Tag, although
  it is up to individual services how they want to handle such
  situations.
Broadening and narrowing are inverse relationships; it would be
  inconsistent and nonsensical for a Tag to be both broader and
  narrower than another.
As an implication, these relationships must not be circular.

Some kinds of tags can indicate other kinds of semantic relationship.
[Setting Tags](#SettingTag) and [Entity Tags](#EntityTag) can use the
  [in canon](#inCanon) relationship to indicate an associated [Canon
  Tag](#CanonTag).
Similarly, [Conceptual Tags](#ConceptualTag) can [involve](#involves)
  Tags of any type.
[Relationship Tags](#RelationshipTag) are a subclass of Conceptual Tags
  which are used to indicate personal relationships beween the
  Character Tags they involve.

### <span class="numero">2.2</span> Tag Systems
{:id="overview.systems"}

Every [Tag](#Tag) necessarily belongs to ([is in](#inSystem)) a [Tag
  System](#TagSystem).
Tag Systems are an abstraction intended to encapsulate information
  about who is responsible for the Tags and who should be trusted
  regarding updates to them.
Tags which belong to different Tag Systems are not equivalent, even if
  they have the same meaning, as their definitions are controlled by
  different people and may diverge over time.
However, Tags may be [close matches](#skos:closeMatch) to tags in other
  Tag Systems, meaning the two should be considered roughly
  interchangeable for matters of sorting and display.

Tag Systems may have an [outbox](#as:outbox), which forms a discovery
  endpoint for various [Activities](#as:Activity) pertaining to the
  Tag System.
See the section on change discovery below for more.

### <span class="numero">2.3</span> Tag Labels
{:id="overview.labels"}

[Tag Labels](#TagLabel) are things with exactly one [literal
  form](#literalForm) and which may [label](#labels) a [Tag](#Tag).
They don’t have to; it is possible to create Tag Labels for which no
  corresponding Tag exists (yet).

On the other hand, every Tag has at least one associated Tag Label: its
  [preferred label](#prefLabel).
A Tag can only have one preferred label, which provides the canonical
  form of the Tag.
However, the literal form of the preferred label may change over time.

Tags may also have [alternative labels](#altLabel) and [hidden
  labels](#hiddenLabel) (available through discovery mechanisms), as
  well as additional Tag Labels which fall under none of these
  categories (typically not easily discoverable).

A [Tag Label Collection](#TagLabelCollection) associates Tag Labels
  (and through them, their associated Tags) with the thing which
  [has](#hasTagLabelCollection) it.
[Ordered Tag Label Collections](#OrderedTagLabelCollection) are a
  subclass of these.
If a thing [has a primary tag label
  collection](#hasPrimaryTagLabelCollection), the Tag Labels (and Tags)
  in that Tag Label Collection should be given precedence over others.
Depending on the service and thing, the primary collection might
  include Tags selected by the creator of the thing, Tags selected by
  administrators, and/or Tags which denote concepts which are most
  prominent in the thing.

## <span class="numero">3</span> Kinds of Tag {#tag_classes}

### <span class="numero">3.1</span> Canon Tags
{:id="tag_classes.canon"}

[Canon Tags](#CanonTag) are used to denote the source materials that a
  thing considers to be canonical.
Canons form the basis for some (although often not all) of a thing’s
  settings, characters, events, or themes.

### <span class="numero">3.2</span> Genre Tags
{:id="tag_classes.genre"}

[Genre Tags](#GenreTag) are used to denote the genres that a thing
  belongs to or is otherwise in conversation with.
These do not have to be large, formally‐recognized genres.

### <span class="numero">3.3</span> Setting Tags
{:id="tag_classes.setting"}

[Setting Tags](#SettingTag) are used to denote the settings which a
  thing depicts, describes, or takes place in.
There are three components to settings, and subclasses of Setting Tag
  have been defined for each :&#x2060;—

[Location Tag](#LocationTag)

: Location Tags are used to denote the spatial aspect of a setting.

[Time Period Tag](#TimePeriodTag)

: Time Period Tags are used to denote the temporal aspect of a setting.

[Universe Tag](#UniverseTag)

: Universe Tags are used to denote the universe to which a setting
    belongs, in contexts where multiple alternate universes may exist.

### <span class="numero">3.4</span> Entity Tags
{:id="tag_classes.entity"}

[Entity Tags](#EntityTag) are used to denote any concept which can
  act, be acted upon, or otherwise change over time.
Entities do not necessarily need to be physical or corporeal.
Subclasses have been defined for specific kinds of entity :&#x2060;—

[Character Tag](#CharacterTag)

: Character Tags are used to denote entities which are personified to
    the extent of being able to form relationships with other
    characters.

[Inanimate Entity Tag](#InanimateEntityTag)

: Inanimate Entity Tags are used to denote any inanimate entity which
    is not a character.

### <span class="numero">3.5</span> Conceptual Tags
{:id="tag_classes.conceptual"}

[Conceptual Tags](#ConceptualTag) are used to denote ideas.
Although this is by no means a requirement, Conceptual Tags can be used
  to describe aspects, modifications, refinements, or relationships
  between one or more other tags; in these cases, they can be indicated
  as [involving](#involves) those other tags.
A Conceptual Tag might be used to highlight a particular trait of a
  character, mark a relationship as unrequited, call out related canons
  which are not otherwise explicitly tagged, or capture remarks from
  a thing’s creator about their feelings or state of mind.

#### <span class="numero">3.5.1</span> Relationship Tags
{:id="tag_classes.conceptual.relationship"}

[Relationship Tags](#RelationshipTag) are a specialized subclass of
  Conceptual Tags which describe all of the involved Tags as being in
  some kind of personal relationship.
Because this requires a certain level of personhood, Relationship Tags
  can only involve Character Tags or other Relationship Tags.
Subclasses have been defined for common forms of relationship :&#x2060;—

[Friendship Tag](#FriendshipTag)

: Friendship Tags are used to denote relationships between friends, or
    relationships which are otherwise characterized by collaboration or
    camaraderie.

[Rivalry Tag](#RivalryTag)

: Rivalry Tags are used to denote relationships between rivals,
    opponents, or nemeses, or relationships which are otherwise
    characterized by competition or antagonism.

[Familial Relationship Tag](#FamilialRelationshipTag)

: Familial Relationship Tags are used to denote relationships between
    family members.
  The definition of “family” is dependent on culture and community
    norms.

[Romantic Relationship Tag](#RomanticRelationshipTag)

: Romantic Relationship Tags are used to denote relationships which
    involve romance.
  The definition of “romance” is dependent on culture and community
    norms.

[Sexual Relationship Tag](#SexualRelationshipTag)

: Sexual Relationship Tags are used to denote relationships which
    involve sexual intercourse.
  The definition of “sexual intercourse” is dependent on culture and
    community norms.

## <span class="numero">4</span> Change Discovery {#discovery}

This section will be written once we have more implementation
  experience!

{% include ontology.markdown name="Tagging" numero="5" %}

{% include ontology.markdown name="TaggingDiscovery" title="Additional Terms for Discovery" numero="6" id_prefix="discovery" %}
