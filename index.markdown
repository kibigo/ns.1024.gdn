---
title: The 1024 Namespace
---

This website hosts dictionaries for various vocabularies within the
  `https://ns.1024.gdn/` namespace.
The term “dictionary” rather than “specification” is used because this
  website hopes to *descriptively* track usage, rather than
  *prescriptively* determine it.

Most terms will take the form
  `https://ns.1024.gdn/⟨Vocabulary⟩/#⟨term⟩`, where `⟨Vocabulary⟩`
  corresponds to one of the following vocabularies :—

<nav markdown="block">
{% assign nameslist = "" | split: " " | slice: 1 %}{% for datapair in site.data %}{% if datapair[1]["@graph"] %}{% unless datapair[1].ignore %}{% assign nameslist = nameslist | push: datapair[0] %}{% endunless %}{% endif %}{% endfor %}{% assign nameslist = nameslist | sort_natural %}{% for name in nameslist %}
[<cite>{{ name }}</cite>]({{ name }}/)
{% assign ontology = site.data[name]["@graph"] | where_exp: "item", 'item["@type"] contains "owl:Ontology"' | first %}{% for comment in ontology.comment %}
: <div lang="{{ comment.language | default: "en" }}" markdown="block">
{% capture newline %}
{% endcapture %}{% assign tabbednewline = newline | append: "    " %}{{ comment.markdown | default: comment | replace: newline, tabbednewline }}
    </div>
{% endfor %}{% endfor %}
</nav>

Many of these vocabularies will also reference other terms under
  different namespaces, for example [<cite>D·C·M·I</cite>][DCMI] and
  [<cite>F·O·A·F</cite>][FOAF].

The <cite>1024</cite> namespace and this website are under stewardship
  of [kibigo!][], who has developed it primarily for her own purposes.

{% include references.markdown %}
