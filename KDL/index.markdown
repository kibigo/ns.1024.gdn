---
title: The K·D·L Vocabulary
links:
- rel: meta
  type: application/ld+json
  href: "owl.jsonld"
---

## Abstract {#abstract}

The <cite>K·D·L</cite> vocabulary provides useful terms for
  representing the data structures of the markup language K·D·L in
  R·D·F.

{% include toc.markdown %}

{% include references.markdown %}

## <span class="numero">1.</span> Introduction {#introduction}

### <span class="numero">1.1</span> Purpose and Scope
{:id="introduction.purpose"}

[K·D·L][KDL] is a minimal language aimed at conveying configurations
  and serializations of data.
At times, it may be necessary to represent a K·D·L document in X·M·L,
  so that it may be processed with ordinary X·M·L tooling.
The most straightforward way of doing this would be to translate the
  K·D·L data structure into R·D·F semantics, and then serialize the
  resulting R·D·F into X·M·L.
The <cite>K·D·L</cite> vocabulary provides the necessary terms for
  this effort.

The namespace for the <cite>K·D·L</cite> vocabulary is
  [`https://ns.1024.gdn/KDL/#`][kdl:].

### <span class="numero">1.2</span> Relationship to Other Vocabularies
{:id="introduction.relationships"}

The <cite>K·D·L</cite> vocabulary only relies on a few terms from the
  core R·D·F vocabularies.

In this document, the following prefixes are used to represent the
  following strings :&#x2060;—

| Prefix | Expansion |
| :----: | :-------: |
| `kdl:` | [`https://ns.1024.gdn/KDL/#`][kdl:] |
| `rdf:` | [`http://www.w3.org/1999/02/22-rdf-syntax-ns#`][rdf:] |
| `xsd:` | [`http://www.w3.org/2001/XMLSchema#`][xsd:] |

### <span class="numero">1.3</span> Term Status
{:id="introduction.status"}

Terms in this document are categorized according to one the five
  following <dfn id="dfn.term_status">term statuses</dfn> :&#x2060;—

stable (✅)

: The term has widely‐accepted usage.

testing (🔬)

: The term is currently being used in some applications, but its exact
    meaning has not yet settled.

dependency (📥)

: The term is not currently being used directly, but its definition
    informs the usage of a stable or testing term.
  (This value is not a part of the original [term status][VS]
    definition.)

unstable (🚧)

: The term is not currently being used in a widespread fashion, but
    experiments into its utility are taking place.
  Its usage may change significantly in the future.

archaic (🦖)

: The term was once widely‐used, but such usage is considered outdated
    and is discouraged.

### <span class="numero">1.4</span> Definitions for Computers
{:id="introduction.computers"}

There is an [Owl][OWL] [ontology for the main vocabulary](./owl.jsonld)
  and a separate [ontology for K·D·L namespaces](./ns.owl.jsonld)
  (which imports the former).
Both are provided in a [J·son‐L·D][JSON-LD] format suitable for viewing
  with, e·g, [Protégé][].
The term definitions seen on this page are derived from the Owl
  definitions.

There also exist two J·son‐L·D contexts :&#x2060;— [a J·son‐L·D
  context for the main vocabulary](./context.jsonld) and [a J·son‐L·D
  context for K·D·L namespaces](./ns.context.jsonld) (which, again,
  makes reference to the former).

Generally, only stable and testing terms will be included in the
  J·son‐L·D contexts for this vocabulary.
If you need to use an unstable term, you will need to define it
  yourself.

### <span class="numero">1.5</span> Using this Vocabulary
{:id="introduction.usage"}

For the following steps, the notation ‹ `(<namespace> localName)` ›
  indicates an X·M·L expanded name in the namespace <i>namespace</i>
  and with a local name of <i>localName</i>.

To <b>map a K·D·L document into X·M·L</b>, create an
  element :&#x2060;—

- With an expanded name of `(<https://ns.1024.gdn/KDL/#> Document)`.

- Whose children are as follows, in order :&#x2060;—

  - An element with an expanded name of
      `(<https://ns.1024.gdn/KDL/#> hasNodes)`, an attribute with an
      expanded name of
      `(<http://www.w3.org/1999/02/22-rdf-syntax-ns#> parseType)` and
      value `Collection`, and result of mapping each toplevel node in
      the document to X·M·L, in order, as children.

To <b>map a K·D·L node into X·M·L</b>, create an element :&#x2060;—

- With an expanded name of `(<https://ns.1024.gdn/KDL/#> Node)`.

- Whose children are as follows, in order :&#x2060;—

  - An element with an expanded name of
    `(<https://ns.1024.gdn/KDL/#> hasName)`, which has an attribute
    with an expanded name of
    `(<http://www.w3.org/1999/02/22-rdf-syntax-ns#> value)` whose value
    is the name of the node.

  - If the node has a type annotation, an element with an expanded name
      of `(<https://ns.1024.gdn/KDL/#> hasTypeAnnotation)`, an
      attribute with an expanded name of
      `(<http://www.w3.org/1999/02/22-rdf-syntax-ns#> parseType)` and
      whose value is `Resource`, and whose children are the result of
      mapping the type annotation into X·M·L.

  - If the node has parameters (arguments or properties), an element
      with an expanded name of
      `(<https://ns.1024.gdn/KDL/#> hasParameters)`, an attribute with
      an expanded name of
      `(<http://www.w3.org/1999/02/22-rdf-syntax-ns#> parseType)` and
      value `Collection`, and result of mapping each parameter to
      X·M·L, in order, as children.

  - If the node has children, an element with an expanded name of
      `(<https://ns.1024.gdn/KDL/#> hasChildren)`, an attribute with an
      expanded name of
      `(<http://www.w3.org/1999/02/22-rdf-syntax-ns#> parseType)` and
      value `Collection`, and result of mapping each child node to
      X·M·L, in order, as children.

To <b>map a K·D·L argument into X·M·L</b>, create an
  element :&#x2060;—

- With an expanded name of `(<https://ns.1024.gdn/KDL/#> Argument)`.

- Whose children are as follows, in order :&#x2060;—

  - If the argument has a type annotation, an element with an expanded
      name of `(<https://ns.1024.gdn/KDL/#> hasTypeAnnotation)`, an
      attribute with an expanded name of
      `(<http://www.w3.org/1999/02/22-rdf-syntax-ns#> parseType)` and
      whose value is `Resource`, and whose children are the result of
      mapping the type annotation into X·M·L.

  - The result of mapping the argument value into X·M·L.

To <b>map a K·D·L property into X·M·L</b>, create an
  element :&#x2060;—

- With an expanded name of `(<https://ns.1024.gdn/KDL/#> Property)`.

- Whose children are as follows, in order :&#x2060;—

  - An element with an expanded name of
    `(<https://ns.1024.gdn/KDL/#> hasName)`, which has an attribute
    with an expanded name of
    `(<http://www.w3.org/1999/02/22-rdf-syntax-ns#> value)` whose value
    is the name of the node.

  - If the property has a type annotation, an element with an expanded
      name of `(<https://ns.1024.gdn/KDL/#> hasTypeAnnotation)`, an
      attribute with an expanded name of
      `(<http://www.w3.org/1999/02/22-rdf-syntax-ns#> parseType)` and
      whose value is `Resource`, and whose children are the result of
      mapping the type annotation into X·M·L.

  - The result of mapping the property value into X·M·L.

To <b>map a non‐null K·D·L value into X·M·L</b>, create an
  element :&#x2060;—

- With an expanded name of `(<https://ns.1024.gdn/KDL/#> value)`.

- With an attribute which has an expanded name of
    `(<http://www.w3.org/1999/02/22-rdf-syntax-ns#> datatype)` and
    whose value is :&#x2060;—

  - `http://www.w3.org/2001/XMLSchema#string` if the value is a string.

  - `http://www.w3.org/2001/XMLSchema#boolean` if the value is a
      boolean.

  - `http://www.w3.org/2001/XMLSchema#integer` if the value is an
      integer.

  - `http://www.w3.org/2001/XMLSchema#decimal` if the value is a number
      but not an integer.

- Whose contents are the canonical lexical mapping of the value into
    the corresponding datatype.
  Note that this does not preserve the exact literal syntax used in
    K·D·L.

To <b>map a null K·D·L value into X·M·L</b>, create an
  element :&#x2060;—

- With an expanded name of `(<https://ns.1024.gdn/KDL/#> isNull)`.

- With an attribute which has an expanded name of
    `(<http://www.w3.org/1999/02/22-rdf-syntax-ns#> datatype)` and
    whose value is `http://www.w3.org/2001/XMLSchema#boolean`.

- Whose contents are the text string `true`.

To <b>map a K·D·L type annotation into X·M·L</b>, create an
  element :&#x2060;—

- With an expanded name of `(<https://ns.1024.gdn/KDL/#> hasName)`.

- Which has an attribute with an expanded name of
    `(<http://www.w3.org/1999/02/22-rdf-syntax-ns#> value)` whose value
    is the name of the type annotation.

A sample serialization is as follows :&#x2060;—

```xml
<!DOCTYPE Document [
  <!ENTITY boolean "http://www.w3.org/2001/XMLSchema#boolean">
  <!ENTITY decimal "http://www.w3.org/2001/XMLSchema#decimal">
  <!ENTITY integer "http://www.w3.org/2001/XMLSchema#integer">
  <!ENTITY string "http://www.w3.org/2001/XMLSchema#string">
]>
<Document
  xmlns="https://ns.1024.gdn/KDL/#"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
>
  <hasNodes rdf:parseType="Collection">
    <Node>
      <hasName rdf:value="package"/>
      <hasChildren rdf:parseType="Collection">
        <Node>
          <hasName rdf:value="name"/>
          <hasParameters rdf:parseType="Collection">
            <Argument>
              <rdf:value rdf:datatype="&string;">my-pkg</rdf:value>
            </Argument>
          </hasParameters>
        </Node>
        <Node>
          <hasName rdf:value="version"/>
          <hasParameters rdf:parseType="Collection">
            <Argument>
              <rdf:value rdf:datatype="&string;">1.2.3</rdf:value>
            </Argument>
          </hasParameters>
        </Node>
        <Node>
          <hasName rdf:value="dependencies"/>
          <hasChildren rdf:parseType="Collection">
            <Node>
              <hasName rdf:value="lodash"/>
              <hasParameters rdf:parseType="Collection">
                <Argument>
                  <rdf:value rdf:datatype="&string;">^3.2.1</rdf:value>
                </Argument>
                <Property>
                  <hasName rdf:value="optional"/>
                  <rdf:value rdf:datatype="&boolean;">true</rdf:value>
                </Property>
                <Property>
                  <hasName rdf:value="alias"/>
                  <rdf:value rdf:datatype="&string;">underscore</rdf:value>
                </Property>
              </hasParameters>
            </Node>
          </hasChildren>
        </Node>
        <Node>
          <hasName rdf:value="scripts"/>
          <hasChildren rdf:parseType="Collection">
            <Node>
              <hasName rdf:value="build"/>
              <hasParameters rdf:parseType="Collection">
                <Argument>
                  <rdf:value rdf:datatype="&string;">
      echo "foo"
      node -c "console.log('hello, world!');"
      echo "foo" > some-file.txt
    </rdf:value>
                </Argument>
              </hasParameters>
            </Node>
          </hasChildren>
        </Node>
      </hasChildren>
    </Node>
  </hasNodes>
</Document>
```

Just for fun, here is the same document as Json‐L·D :&#x2060;—

```json
{ "@context": "https://ns.1024.gdn/KDL/context.jsonld"
, "@type": "Document"
, "hasNodes":
  [ { "@type": "Node"
    , "hasName": { "value": "package" }
    , "hasChildren":
      [ { "@type": "Node"
        , "hasName": { "value": "name" }
        , "hasParameters":
          [ { "@type": "Argument"
            , "value":
              { "@type": "string"
              , "@value": "my-pkg" } } ] }
      , { "@type": "Node"
        , "hasName": { "value": "version" }
        , "hasParameters":
          [ { "@type": "Argument"
            , "value":
              { "@type": "string"
              , "@value": "1.2.3" } } ] }
      , { "@type": "Node"
        , "hasName": { "value": "dependencies" }
        , "hasChildren":
          [ { "@type": "Node"
            , "hasName": { "value": "lodash" }
            , "hasParameters":
              [ { "@type": "Argument"
                , "value":
                  { "@type": "string"
                  , "@value": "^3.2.1" } }
              , { "@type": "Property"
                , "hasName": { "value": "optional" }
                , "value":
                  { "@type": "boolean"
                  , "@value": "true" } }
              , { "@type": "Property"
                , "hasName": { "value": "alias" }
                , "value":
                  { "@type": "string"
                  , "@value": "underscore" } } ] } ] }
      , { "@type": "Node"
        , "hasName": { "value": "scripts" }
        , "hasChildren":
          [ { "@type": "Node"
            , "hasName": { "value": "build" }
            , "hasParameters":
              [ { "@type": "Argument"
                , "value":
                  { "@type": "string"
                  , "@value": "\n      echo \"foo\"\n      node -c \"console.log('hello, world!');\"\n      echo \"foo\" > some-file.txt\n    " } } ] } ] } ] } ] }
```

Namespace support can be added by simply supplying a [expanded
  form](#expandedForm), [local part](#localPart),
  [namespace](#namespace), and [prefix](#prefix) on every [has
  name](#hasName) element which indicates a [Namespaced
  Identifier](#NamespacedIdentifier).

{% include ontology.markdown name="KDL" numero="3" %}

{% include ontology.markdown name="KDLNS" title="Namespaces in K·D·L" numero="4" ontology_prefix="kdl:" id_prefix="ns" %}
