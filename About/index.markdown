---
title: The About Vocabulary
links:
- rel: alternate
  type: application/ld+json
  href: "owl.jsonld"
---

## Abstract {#abstract}

The <cite>About</cite> vocabulary provides useful terms for “about” pages
describing people or things.

{% include toc.markdown %}

{% include references.markdown %}

## <span class="numero">1.</span> Introduction {#introduction}

### <span class="numero">1.1</span> Purpose and Scope
{: id="introduction.purpose"}

The namespace for the <cite>About</cite> vocabulary is
  [`https://ns.1024.gdn/About/#`][about:].

### <span class="numero">1.2</span> Relationship to Other Vocabularies
{: id="introduction.relationships"}

<cite>About</cite> is conceived of as an extension to
  [<cite>F·O·A·F</cite>][FOAF], and is expected to be used in
  conjunction with the latter.

In this document, the following prefixes are used to represent the
  following strings :—

| Prefix | Expansion |
| :----: | :-------: |
| `about:` | [`https://ns.1024.gdn/About/#`][about:] |
| `foaf:` | [`http://xmlns.com/foaf/0.1/`][foaf:] |
| `xsd:` | [`http://www.w3.org/2001/XMLSchema#`][xsd:] |

{% include ontology.markdown name="About" numero="2" %}
