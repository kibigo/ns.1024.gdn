"@context":
  as: https://www.w3.org/ns/activitystreams#
  prov: http://www.w3.org/ns/prov#
  skos: http://www.w3.org/2004/02/skos/core#
  sync: https://ns.1024.gdn/Sync/#
  tagging: https://ns.1024.gdn/Tagging/#

"ignore": true

"@graph":

# The Tagging Discovery Vocabulary ####################################

- "@id": "tagging:discovery"
  "@type":
  - owl:Ontology
  imports:
  - https://ns.1024.gdn/Tagging/owl.jsonld
  label: The Tagging Discovery Vocabulary
  comment:
  - language: en
    markdown: |
      A vocabulary for the discovery of “tags”, as defined by
      [<cite>Tagging</cite>][1024/Tagging].

# Classes #############################################################

- "@id": as:Object
  "@type":
  - owl:Class
  label: Object
  comment:
  - language: en
    markdown: |
      Any [ActivityStreams][ActivityStreams] object.
  is defined by:
  - https://www.w3.org/TR/activitystreams-vocabulary
  term status: dependency

- "@id": as:Activity
  "@type":
  - owl:Class
  label: Activity
  equivalent class:
  - prov:Activity
  subclass of:
  - as:Object
  comment:
  - language: en
    markdown: |
      An action or event.

      [Tag Activities](#TagActivity) are used to denote changes to a
        [Tag System](#TagSystem).
  is defined by:
  - https://www.w3.org/TR/activitystreams-vocabulary
  term status: dependency

- "@id": as:Create
  "@type":
  - owl:Class
  label: Create
  subclass of:
  - as:Activity
  - "@type": owl:Restriction
    on property: sync:unstates
    cardinality: 0
  comment:
  - language: en
    markdown: |
      An [Activity](#as:Activity) which creates a new thing, identified
        by its [object](#as:object).

      Implementations should *not* trust metadata directly attached to
        the object of this Activity, as it may not be contemporaneous.
      Instead, they should look for any [stated](#sync:stated)
        [Statements](#rdf:Statement).
  is defined by:
  - https://www.w3.org/TR/activitystreams-vocabulary
  term status: stable

- "@id": as:Delete
  "@type":
  - owl:Class
  label: Delete
  subclass of:
  - as:Activity
  - "@type": owl:Restriction
    on property: sync:states
    cardinality: 0
  comment:
  - language: en
    markdown: |
      An [Activity](#as:Activity) which destroys an existing thing,
        identified by its [object](#as:object).

      Implementations should assume that any
        [Statements](#rdf:Statement) associated with the object of this
        Activity have been invalidated.
      The object *may* be replaced with a [Tombstone](#as:Tombstone).
  is defined by:
  - https://www.w3.org/TR/activitystreams-vocabulary
  term status: stable

- "@id": as:Update
  "@type":
  - owl:Class
  label: Update
  subclass of:
  - as:Activity
  comment:
  - language: en
    markdown: |
      An [Activity](#as:Activity) which updates an existing thing,
        identified by its [object](#as:object).

      Implementations should *not* trust metadata directly attached to
        the object of this Activity, as it may not be contemporaneous.
      Instead, they should look for any [stated](#sync:states) or
        [unstated](#sync:unstates) [Statements](#rdf:Statement).
  is defined by:
  - https://www.w3.org/TR/activitystreams-vocabulary
  term status: stable

- "@id": as:Collection
  "@type":
  - owl:Class
  label: Collection
  subclass of:
  - as:Object
  comment:
  - language: en
    markdown: |
      An [Object](#as:Object) which contains Objects as its
        [items](#as:items).

      If a Collection is an [Ordered Collection](#as:Collection), then
        its items are a single [List](#rdf:List) containing the ordered
        items.
      However, if a Collection is *not* an Ordered Collection, then its
        items are the (potentially multiple) items themselves.
      This is confusing and ontologically problematic, but it is how
        things are defined in the [<cite>Activity</cite>][Activity]
        vocabulary.
  is defined by:
  - https://www.w3.org/TR/activitystreams-vocabulary
  term status: dependency

- "@id": as:CollectionPage
  "@type":
  - owl:Class
  label: Collection Page
  subclass of:
  - as:Collection
  comment:
  - language: en
    markdown: |
      A [Collection](#as:Collection) which is a subset of a larger
        Collection.
  is defined by:
  - https://www.w3.org/TR/activitystreams-vocabulary
  term status: dependency

- "@id": as:OrderedCollection
  "@type":
  - owl:Class
  label: Ordered Collection
  subclass of:
  - as:Collection
  - "@type": owl:Restriction
    on property: as:items
    qualified cardinality: 1
    on class: rdf:List
  comment:
  - language: en
    markdown: |
      A [Collection](#as:Collection) whose [items](#as:items) are
        given as an ordered [List](#as:List).
  is defined by:
  - https://www.w3.org/TR/activitystreams-vocabulary
  term status: stable

- "@id": as:OrderedCollectionPage
  "@type":
  - owl:Class
  label: Ordered Collection Page
  subclass of:
  - as:CollectionPage
  - as:OrderedCollection
  comment:
  - language: en
    markdown: |
      An [Ordered Collection](#as:OrderedCollection) which is a subset
        of a larger Ordered Collection.
  is defined by:
  - https://www.w3.org/TR/activitystreams-vocabulary
  term status: stable

- "@id": as:Tombstone
  "@type":
  - owl:Class
  label: Tombstone
  subclass of:
  - as:Object
  comment:
  - language: en
    markdown: |
      A placeholder [Object](#as:Object) for one which has been
        deleted.
  is defined by:
  - https://www.w3.org/TR/activitystreams-vocabulary
  term status: stable

- "@id": rdf:Statement
  "@type":
  - owl:Class
  label: Statement
  comment:
  - language: en
    markdown: |
      A re·ified R·D·F statement (with [subject](#rdf:subject),
        [object](#rdf:object), [predicate](#rdf:predicate)…).
  is defined by:
  - https://www.w3.org/TR/rdf-schema
  term status: dependency

- "@id": sync:SyncActivity
  "@type":
  - owl:Class
  label: Sync Activity
  subclass of:
  - as:Activity
  - "@type": owl:Restriction
    on property: as:context
    max cardinality: 1
  - "@type": owl:Restriction
    on property: as:endTime
    cardinality: 1
  - "@type": owl:Restriction
    on property: as:object
    cardinality: 1
  comment:
  - language: en
    markdown: |
      An [Activity](#as:Activity) which [states](#sync:states) and
        [unstates](#sync:unstates) [Statements](#rdf:Statement).
      The [object](#as:object) of a Sync Activity must be the same as
        the [subject](#rdf:subject) of its Statements.

      Sync Activities must specify an [end time](#as:endTime), which
        denotes the time at which the stated and unstated statements
        took effect.
      They should also [have a context](#as:context).

      Sync Activities typically will also belong to another Activity
        subclass, like [Create](#as:Create), [Update](#as:Update), or
        [Delete](#as:Delete).
      They can be annotated with [labels](#rdfs:label) and
        [comments](#rdfs:comment).
  is defined by:
  - https://ns.1024.gdn/Sync/
  term status: testing

- "@id": tagging:TagActivity
  "@type":
  - owl:Class
  label: Tag Activity
  subclass of:
  - sync:SyncActivity
  - "@type": owl:Restriction
    on property: as:context
    qualified cardinality: 1
    on class: tagging:TagSystem
  comment:
  - language: en
    markdown: |
      A [Sync Activity](#sync:SyncActivity) with a [Tag
        System](#TagSystem) [context](#as:context) of a which
        [states](#sync:states) and [unstates](#sync:unstates)
        [Statements](#rdf:Statement).

      Typically Tag Activities will also belong to another Activity
        subclass, like [Create](#as:Create), [Update](#as:Update), or
        [Delete](#as:Delete).
      They can be annotated with [labels](#rdfs:label) and
        [comments](#rdfs:comment).
  is defined by:
  - https://ns.1024.gdn/Tagging/
  term status: testing

# Object Properties ###################################################

- "@id": as:context
  "@type":
  - owl:ObjectProperty
  label: has context
  domain:
  - as:Object
  range:
  - as:Object
  comment:
  - language: en
    markdown: |
      A context in which this [Object](#as:Object) exists.

      [Tag Activities](#TagActivity) must have a [Tag
        System](#TagSystem) as their context.
  is defined by:
  - https://www.w3.org/TR/activitystreams-vocabulary
  term status: stable

- "@id": as:first
  "@type":
  - owl:ObjectProperty
  label: has first page
  domain:
  - as:Collection
  range:
  - as:CollectionPage
  comment:
  - language: en
    markdown: |
      If this is a [Collection Page](#as:CollectionPage), the furthest
        preceding Collection Page.
      If it is a [Collection](#as:Collection) but *not* a Collection
        Page, the first Collection Page of the Collection.
      This is semantically confusing but it’s how [ActivityStreams][]
        defined it.
  is defined by:
  - https://www.w3.org/TR/activitystreams-vocabulary
  term status: stable

- "@id": as:items
  "@type":
  - owl:ObjectProperty
  label: has items
  domain:
  - as:Collection
  range:
  - "@type": owl:Class
    intersection of:
    - as:Object
    - rdf:List
  comment:
  - language: en
    markdown: |
      The items belonging to this [Collection](#as:Collection).

      When present on an [Ordered Collection](#as:OrderedCollection),
        the items are given as a single ordered [List](#rdf:List).
      Otherwise, the items are given directly.
  is defined by:
  - https://www.w3.org/TR/activitystreams-vocabulary
  term status: stable

- "@id": as:last
  "@type":
  - owl:ObjectProperty
  label: has last page
  domain:
  - as:Collection
  range:
  - as:CollectionPage
  comment:
  - language: en
    markdown: |
      If this is a [Collection Page](#as:CollectionPage), the furthest
        succeeding Collection Page.
      If it is a [Collection](#as:Collection) but *not* a Collection
        Page, the last Collection Page of the Collection.
      This is semantically confusing but it’s how [ActivityStreams][]
        defined it.
  is defined by:
  - https://www.w3.org/TR/activitystreams-vocabulary
  term status: stable

- "@id": as:next
  "@type":
  - owl:ObjectProperty
  label: has next page
  domain:
  - as:CollectionPage
  range:
  - as:CollectionPage
  comment:
  - language: en
    markdown: |
      The [Collection Page](#as:CollectionPage) which follows this one.
  is defined by:
  - https://www.w3.org/TR/activitystreams-vocabulary
  term status: stable

- "@id": as:object
  "@type":
  - owl:ObjectProperty
  label: has object
  domain:
  - as:Activity
  range:
  - as:Object
  comment:
  - language: en
    markdown: |
      The direct object of this [Activity](#as:Activity).

      For [Tag Activities](#TagActivity), the object must also be the
        [subject](#rdf:subject) of any [stated](#sync:states) or
        [unstated](#sync:unstates) [Statements](#rdf:Statement).
  is defined by:
  - https://www.w3.org/TR/activitystreams-vocabulary
  term status: stable

- "@id": as:outbox
  "@type":
  - owl:ObjectProperty
  label: has outbox
  domain:
  - as:Object
  range:
  - as:OrderedCollection
  comment:
  - language: en
    markdown: |
      An [Ordered Collection](#as:OrderedCollection) of
        [Activities](#as:Activity) created by this [Object](#as:Object)
        or those that control it.

      When applied to a [Tag System](#TagSystem), the Ordered
        Collection must be paginated.
      It should also [have a rights statement](#dcterms:rights)
        indicating the terms of use and re·use for the Tag System and
        its associated [Tags](#Tag).
  is defined by:
  - https://www.w3.org/TR/activitystreams-vocabulary
  term status: stable

- "@id": as:partOf
  "@type":
  - owl:ObjectProperty
  label: part of
  domain:
  - as:CollectionPage
  range:
  - as:Collection
  comment:
  - language: en
    markdown: |
      The [Collection](#as:Collection) that this [Collection
        Page](#as:CollectionPage) forms part of.
  is defined by:
  - https://www.w3.org/TR/activitystreams-vocabulary
  term status: stable

- "@id": as:prev
  "@type":
  - owl:ObjectProperty
  label: has previous page
  domain:
  - as:CollectionPage
  range:
  - as:CollectionPage
  comment:
  - language: en
    markdown: |
      The [Collection Page](#as:CollectionPage) which precedes this
        one.
  is defined by:
  - https://www.w3.org/TR/activitystreams-vocabulary
  term status: stable

- "@id": prov:influenced
  "@type":
  - owl:ObjectProperty
  label: influenced
  inverse of:
  - prov:wasInfluencedBy
  domain:
  - "@type": owl:Class
    union of:
    - prov:Activity
    - prov:Agent
    - prov:Entity
  range:
  - "@type": owl:Class
    union of:
    - prov:Activity
    - prov:Agent
    - prov:Entity
  comment:
  - language: en
    markdown: |
      An [Activity](#prov:Activity), [Agent](#prov:Agent), or
        [Entity](#prov:Entity) which this Activity, Agent, or Entity
        somehow influenced.

      This is an incredibly broad relation.
  is defined by:
  - https://www.w3.org/TR/prov-o
  term status: dependency

- "@id": prov:generated
  "@type":
  - owl:ObjectProperty
  label: generated
  subproperty of:
  - prov:influenced
  domain:
  - prov:Activity
  range:
  - prov:Entity
  comment:
  - language: en
    markdown: |
      An [Entity](#prov:Entity) which was created by this
        [Activity](#prov:Activity).
  is defined by:
  - https://www.w3.org/TR/prov-o
  term status: dependency

- "@id": prov:invalidated
  "@type":
  - owl:ObjectProperty
  label: invalidated
  subproperty of:
  - prov:influenced
  domain:
  - prov:Activity
  range:
  - prov:Entity
  comment:
  - language: en
    markdown: |
      An [Entity](#prov:Entity) which was destroyed or made invalid by
        this [Activity](#prov:Activity).
  is defined by:
  - https://www.w3.org/TR/prov-o
  term status: dependency

- "@id": rdf:predicate
  "@type":
  - owl:ObjectProperty
  label: predicate
  domain:
  - rdf:Statement
  comment:
  - language: en
    markdown: |
      The predicate of this [Statement](#rdf:Statement).
  is defined by:
  - https://www.w3.org/TR/rdf-schema
  term status: stable

- "@id": rdf:subject
  "@type":
  - owl:ObjectProperty
  label: subject
  property chain axiom:
  - "@list":
    - "@type": owl:ObjectProperty
      inverse of: tagging:states
    - as:object
  - "@list":
    - "@type": owl:ObjectProperty
      inverse of: tagging:unstates
    - as:object
  domain:
  - rdf:Statement
  comment:
  - language: en
    markdown: |
      The subject of this [Statement](#rdf:Statement).

      This needn’t be declared explicitly for Statements which are
        [stated](#sync:states) or [unstated](#sync:unstates) on a [Sync
        Activity](#sync:SyncActivity), as it is required to be the same
        as that Sync Activity’s [object](#as:object).
  is defined by:
  - https://www.w3.org/TR/rdf-schema
  term status: dependency

- "@id": sync:states
  "@type":
  - owl:ObjectProperty
  label: states
  subproperty of:
  - prov:generated
  domain:
  - as:Activity
  range:
  - rdf:Statement
  property disjoint with:
  - sync:unstates
  comment:
  - language: en
    markdown: |
      A [Statement](#rdf:Statement) which was created by this
        [Activity](#as:Activity).

      The [subject](#rdf:subject) of the Statement is necessarily the
        [object](#as:object) of this Activity.
  is defined by:
  - https://ns.1024.gdn/Sync/
  term status: testing

- "@id": sync:unstates
  "@type":
  - owl:ObjectProperty
  label: unstates
  subproperty of:
  - prov:invalidated
  domain:
  - prov:Activity
  range:
  - rdf:Statement
  property disjoint with:
  - sync:states
  comment:
  - language: en
    markdown: |
      An [Statement](#rdf:Statement) which was destroyed or made
        invalid by this [Activity](#as:Activity).

      The [subject](#rdf:subject) of the Statement is necessarily the
        [object](#as:object) of this Activity.
  is defined by:
  - https://ns.1024.gdn/Sync/
  term status: testing

# Data Properties #####################################################

- "@id": as:endTime
  "@type":
  - owl:DatatypeProperty
  - owl:FunctionalProperty
  label: end time
  domain:
  - as:Object
  range:
  - xsd:dateTime
  comment:
  - language: en
    markdown: |
      The time at which this [Object](#as:Object) ended or
        [Activity](#as:Activity) concluded.
  is defined by:
  - https://www.w3.org/TR/activitystreams-vocabulary
  term status: stable

- "@id": skos:altLabel
  "@type":
  - owl:DatatypeProperty
  label: alternative label
  property chain axiom:
  - "@list":
    - skosxl:altLabel
    - skosxl:literalForm
  domain:
  - skos:Concept
  range:
  - rdf:PlainLiteral
  comment:
  - language: en
    markdown: |
      An alternative string label for this [Concept](#skos:Concept).

      This property is used in the predicate of
        [Statements](#rdf:Statement) to apply labelling information to
        [Tags](#Tag) in [Tag Activities](#TagActivity).
      It is implied, and usually not explicitly stated, in other cases.
  is defined by:
  - https://www.w3.org/TR/skos-reference
  term status: stable

- "@id": skos:hiddenLabel
  "@type":
  - owl:DatatypeProperty
  label: hidden label
  property chain axiom:
  - "@list":
    - skosxl:hiddenLabel
    - skosxl:literalForm
  domain:
  - skos:Concept
  range:
  - rdf:PlainLiteral
  comment:
  - language: en
    markdown: |
      A hidden string label for this [Concept](#skos:Concept).

      This property is used in the predicate of
        [Statements](#rdf:Statement) to apply labelling information to
        [Tags](#Tag) in [Tag Activities](#TagActivity).
      It is implied, and usually not explicitly stated, in other cases.
  is defined by:
  - https://www.w3.org/TR/skos-reference
  term status: stable

- "@id": skos:prefLabel
  "@type":
  - owl:DatatypeProperty
  label: preferred label
  property chain axiom:
  - "@list":
    - skosxl:prefLabel
    - skosxl:literalForm
  domain:
  - skos:Concept
  range:
  - rdf:PlainLiteral
  comment:
  - language: en
    markdown: |
      The preferred string label for this [Concept](#skos:Concept).

      This property is used in the predicate of
        [Statements](#rdf:Statement) to apply labelling information to
        [Tags](#Tag) in [Tag Activities](#TagActivity).
      It is implied, and usually not explicitly stated, in other cases.
  is defined by:
  - https://www.w3.org/TR/skos-reference
  term status: stable

# Annotation Properties ###############################################

- "@id": rdf:object
  "@type":
  - owl:AnnotationProperty
  label: object
  domain:
  - rdf:Statement
  comment:
  - language: en
    markdown: |
      The object of this [Statement](#rdf:Statement).

      (This property is classified as an annotation property because it
        could be either have either a node or literal value.)
  is defined by:
  - https://www.w3.org/TR/rdf-schema
  term status: stable
