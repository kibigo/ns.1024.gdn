"@context":
  about: https://ns.1024.gdn/About/#
  foaf: http://xmlns.com/foaf/0.1/

"@graph":

# The About Vocabulary ################################################

- "@id": "about:"
  "@type":
  - owl:Ontology
  imports: "foaf:"
  label: The About Vocabulary
  comment:
  - language: en
    markdown: |
      A vocabulary providing useful terms for “about” pages regarding
      some thing, implemented as a set of extentions to
      [<cite>F·O·A·F</cite>][FOAF].

# Classes #############################################################

- "@id": foaf:Agent
  "@type":
  - owl:Class
  label: Agent
  comment:
  - language: en
    markdown: |
      A “thing which does stuff”. Agents can be people, software,
      forces of nature…
  is defined by:
  - http://xmlns.com/foaf/spec/

- "@id": foaf:Document
  "@type":
  - owl:Class
  label: Document
  disjoint with:
  - foaf:Project
  comment:
  - language: en
    markdown: |
      An intentionally‐broad category encompassing any kind of
      non‐ephemeral, citable work, including books, images, audiovisual
      material, games, postage stamps, anything which might be stored
      on a computer…
  is defined by:
  - http://xmlns.com/foaf/spec/

- "@id": foaf:Project
  "@type":
  - owl:Class
  label: Project
  disjoint with:
  - foaf:Agent
  - foaf:Document
  comment:
  - language: en
    markdown: |
      An intentionally‐broad category encompassing those (abstract)
      things which might be conceived of as “projects”. Projects are
      not [Agents](#foaf:Agent) (they are controlled by Agents), nor
      [Documents](#foaf:Document) (although they may result in
      Documents).
  is defined by:
  - http://xmlns.com/foaf/spec/

# Object Properties ###################################################

- "@id": about:isFamiliarWith
  "@type":
  - owl:ObjectProperty
  label: is familiar with
  domain:
  - foaf:Agent
  range:
  - foaf:Document
  comment:
  - language: en
    markdown: |
      A [Document](#foaf:Document) with which this [Agent](#foaf:Agent)
      has familiarity.

      While this property is often used to describe formal or technical
      documents, like specifications, it can also denote other culteral
      artefacts, like fictional stories.
  is defined by:
  - https://ns.1024.gdn/About/

# Data Properties #####################################################

- "@id": about:taskCount
  "@type":
  - owl:DatatypeProperty
  - owl:FunctionalProperty
  label: task count
  domain:
  - foaf:Project
  range:
  - xsd:nonNegativeInteger
  comment:
  - language: en
    markdown: |
      The total number of tasks, whether outstanding or completed,
      which comprise this [Project](#foaf:Project). This property is
      intended for use with [task progress](#taskProgress) to roughly
      indicate the current progress of a Project towards completion.
  is defined by:
  - https://ns.1024.gdn/About/

- "@id": about:taskProgress
  "@type":
  - owl:DatatypeProperty
  - owl:FunctionalProperty
  label: task progress
  domain:
  - foaf:Project
  range:
  - xsd:nonNegativeInteger
  comment:
  - language: en
    markdown: |
      The number of tasks which have been completed for this
      [Project](#foaf:Project). This property is intended for use with
      [task count](#taskCount) to roughly indicate the current progress
      of a Project towards completion.
  is defined by:
  - https://ns.1024.gdn/About/
