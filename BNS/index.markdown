---
title: The B·N·S Vocabulary
links:
- rel: alternate
  type: application/ld+json
  href: "owl.jsonld"
---

## Abstract {#abstract}

The Branching Notational System (B·N·S) provides a means of
  categorizing works and their constituent parts in an ordered,
  branching tree.

{% include toc.markdown %}

{% include references.markdown %}

## <span class="numero">1.</span> Introduction {#introduction}

### <span class="numero">1.1</span> Purpose and Scope
{: id="introduction.purpose"}

The <cite>B·N·S</cite> vocabulary is the culmination of over a decade
  of work attempting to catalogue multiple revisions of (primarily,
  altho not exclusively, textual) works with potentially complex
  inter·relationships and internal structure.
The initial version used [semver][Semver]‐inspired numeric identifiers
  for <i>project</i>, <i>version</i>, and <i>draft</i>; this was soon
  expanded to allow for non‐numeric identifiers as well as
  <i>volume</i> and <i>part</i> divisions.
The current system uses a strict branching structure of potentially as
  many as 12 levels, which are as follows :&#x2060;—

 +  <b><i>[Corpus](#Corpus)</i></b>
 +  <b>[Project](#Project)</b>
 +  [Book](#Book)
 +  <i>[Concept](#Concept)</i>
 +  [Volume](#Volume)
 +  [Arc](#Arc)
 +  <i>[Version](#Version)</i>
 +  [Side](#Side)
 +  [Chapter](#Chapter)
 +  <i>[Draft](#Draft)</i>
 +  [Section](#Section)
 +  [Verse](#Verse)

This system is known as the Branching Notational System (B·N·S), and
  the <cite>B·N·S</cite> vocabulary provides the terms necessary for
  describing it.

The namespace for the <cite>B·N·S</cite> vocabulary is
  [`https://ns.1024.gdn/BNS/#`][bns:].

### <span class="numero">1.2</span> Relationship to Other Vocabularies
{: id="introduction.relationships"}

The <cite>B·N·S</cite> is built upon various core metadata vocabularies
  common to the web, like [<cite>D·C·M·I</cite>][DCMI], and makes use
  of [<cite>P·C·D·M</cite>][PCDM] for structural modelling.

In this document, the following prefixes are used to represent the
  following strings :—

| Prefix | Expansion |
| :----: | :-------: |
| `bns:` | [`https://ns.1024.gdn/BNS/#`][bns:] |
| `dc11:` | [`http://purl.org/dc/elements/1.1/`][dc11:] |
| `dcmitype:` | [`http://purl.org/dc/dcmitype/`][dcmitype:] |
| `dcterms:` | [`http://purl.org/dc/terms/`][dcterms:] |
| `ore:` | [`http://www.openarchives.org/ore/terms/`][ore:] |
| `owl:` | [`http://www.w3.org/2002/07/owl#`][owl:] |
| `pcdm:` | [`http://pcdm.org/models#`][pcdm:] |
| `rdf:` | [`http://www.w3.org/1999/02/22-rdf-syntax-ns#`][rdf:] |
| `rdfs:` | [`http://www.w3.org/2000/01/rdf-schema#`][rdfs:] |
| `rel:` | [`http://www.iana.org/assignments/relation/`][rel:] |
| `xsd:` | [`http://www.w3.org/2001/XMLSchema#`][xsd:] |

### <span class="numero">1.3</span> Term Status
{:id="introduction.status"}

Terms in this document are categorized according to one the five
  following <dfn id="dfn.term_status">term statuses</dfn> :&#x2060;—

stable (✅)

: The term has widely‐accepted usage.

testing (🔬)

: The term is currently being used in some applications, but its exact
    meaning has not yet settled.

dependency (📥)

: The term is not currently being used directly, but its definition
    informs the usage of a stable or testing term.
  (This value is not a part of the original [term status][VS]
    definition.)

unstable (🚧)

: The term is not currently being used in a widespread fashion, but
    experiments into its utility are taking place.
  Its usage may change significantly in the future.

archaic (🦖)

: The term was once widely‐used, but such usage is considered outdated
    and is discouraged.

### <span class="numero">1.4</span> Definitions for Computers
{:id="introduction.computers"}

There is an [Owl][OWL] [ontology for the <cite>B·N·S</cite>
  vocabulary](./owl.jsonld) available in a [J·son‐L·D][JSON-LD] format
  suitable for viewing with, e·g, [Protégé][].
The term definitions seen on this page are derived from the Owl
  definitions.

### <span class="numero">1.5</span> Using this Vocabulary
{:id="introduction.usage"}

The <cite>B·N·S</cite> vocabulary is designed for use with R·D·F/X·M·L.
The [`marrus-sh/Corpora`][marrus-sh/Corpora] repository on Github
  provides tools for creating and rendering B·N·S corpora.
Properties from the <cite>B·N·S</cite> namespace may also be useful in
  other contexts, for example in federating works between systems.

## <span class="numero">2</span> Overview of Concepts {#overview}

### <span class="numero">2.1</span> Corpora and Pseuds
{:id="overview.corpora"}

<div style="Text-Align: Center"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" style="background:#FFFFFF;" version="1.1" viewBox="0 0 630 195"><defs/><g><g id="elem_Corpus"><rect fill="#F1F1F1" height="81.1875" style="stroke:#181818;stroke-width:0.5;" width="231" x="27.5" y="7"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="50" x="118" y="21.9951">Corpus</text><line style="stroke:#181818;stroke-width:1.0;" x1="27.5" x2="258.5" y1="27.2969" y2="27.2969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="57" x="38" y="42.292">full title</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="123" x="110.5" y="42.292"> "My Corpus"@en</text><line style="stroke:#181818;stroke-width:1.0;" x1="105.5" x2="105.5" y1="27.2969" y2="47.5938"/><line style="stroke:#181818;stroke-width:1.0;" x1="27.5" x2="258.5" y1="47.5938" y2="47.5938"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="61" x="36" y="62.5889">date tag</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="95" x="110.5" y="62.5889">"2023-07-23"</text><line style="stroke:#181818;stroke-width:1.0;" x1="105.5" x2="105.5" y1="47.5938" y2="67.8906"/><line style="stroke:#181818;stroke-width:1.0;" x1="27.5" x2="258.5" y1="67.8906" y2="67.8906"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="68" x="32.5" y="82.8857">name tag</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="143" x="110.5" y="82.8857">"bns.example.com"</text><line style="stroke:#181818;stroke-width:1.0;" x1="105.5" x2="105.5" y1="67.8906" y2="88.1875"/></g><g id="elem_Pseud"><rect fill="#F1F1F1" height="40.5938" style="stroke:#181818;stroke-width:0.5;" width="192" x="431" y="27.5"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="42" x="506" y="42.4951">Pseud</text><line style="stroke:#181818;stroke-width:1.0;" x1="431" x2="623" y1="47.7969" y2="47.7969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="57" x="436" y="62.792">full title</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="115" x="503" y="62.792"> "My Pseud"@en</text><line style="stroke:#181818;stroke-width:1.0;" x1="498" x2="498" y1="47.7969" y2="68.0938"/></g><g id="elem_Agent"><rect fill="#F1F1F1" height="40.5938" style="stroke:#181818;stroke-width:0.5;" width="144" x="455" y="127.5"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="44" x="505" y="142.4951">Agent</text><line style="stroke:#181818;stroke-width:1.0;" x1="455" x2="599" y1="147.7969" y2="147.7969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="57" x="460" y="162.792">full title</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="67" x="527" y="162.792">"Me"@en</text><line style="stroke:#181818;stroke-width:1.0;" x1="522" x2="522" y1="147.7969" y2="168.0938"/></g><g id="elem_Description"><rect fill="#F1F1F1" height="40.5938" style="stroke:#181818;stroke-width:0.5;" width="272" x="7" y="147.5"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="84" x="101" y="162.4951">Description</text><line style="stroke:#181818;stroke-width:1.0;" x1="7" x2="279" y1="167.7969" y2="167.7969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="65" x="12" y="182.792">contents</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="187" x="87" y="182.792">"My amazing corpus."@en</text><line style="stroke:#181818;stroke-width:1.0;" x1="82" x2="82" y1="167.7969" y2="188.0938"/></g><!--link Corpus to Pseud--><g id="link_Corpus_Pseud"><path codeLine="16" d="M266.707,47.5 C315.332,47.5 370.758,47.5 417.849,47.5 " fill="none" id="Corpus-to-Pseud" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="422.963,47.5,413.963,43.5,417.963,47.5,413.963,51.5,422.963,47.5" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="74" x="318" y="43.5669">is corpus of</text></g><!--link Corpus to Description--><g id="link_Corpus_Description"><path codeLine="17" d="M143,92.1356 C143,107.4073 143,124.1029 143,137.8898 " fill="none" id="Corpus-to-Description" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="143,143.2162,147,134.2162,143,138.2162,139,134.2162,143,143.2162" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="97" x="83" y="122.3169">has description</text></g><!--link Pseud to Agent--><g id="link_Pseud_Agent"><path codeLine="18" d="M527,71.831 C527,85.752 527,103.3169 527,118.0181 " fill="none" id="Pseud-to-Agent" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="527,123.2928,531,114.2928,527,118.2928,523,114.2928,527,123.2928" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="70" x="480.5" y="102.0669">is pseud of</text></g><!--SRC=[TOzDIWGn48NtEKMHVJT3pq8Oi1dHxO1NA9Fgda1-Gb81Ht7XIRoR9p6c6wT1Pfllg_VoZfanSN5MM9eOE40oyvb1cqIAJV32OOJ7a6B9y2u0fc8ji65BS3z-Vtx9qsN5yaY-2XgPW76k6EIm6_RTxguRzh8YZ-wANdpkwHLTjDIhuAJuQ5NFcOg-sTHeArhiXvay_xT1dcXZFL5MoSJVESrLmNEzoujv0NJuPlmCgkteryjrTDUDI-q1J5uL2DCV7hVn1pXZ1drz4EsqMijNMqPiJpMYzkZYx0y0]--></g></svg></div>

In short, the B·N·S describes [Corpora](#Corpus), which are collections
  of works by a particular [Pseud](#Pseud) organized into a particular
  branching structure.
The term <i>Corpus</i> refers not just to the works but also to the
  branching structure itself:
Two Corpora which contain exactly the same works might still be
  different if they have different structures.
Corpora are formally identified by a combination of a [name
  tag](#nameTag) and a [date tag](#dateTag), which together should
  signify a domain name controlled by the person managing the Corpus at
  the given date.
This can be represented using the syntax of a [Tag U·R·I][RFC4151] as
  follows :&#x2060;—

    tag:<nameTag>,<dateTag>:B@N/

—&#x2060;: where `B@N/` is a magic string meant to indicate that the
  tagged resource is a <cite>B·N·S</cite> Corpus.

The combination of name tag and date tag help to identify whether
  backwards‐incompatible changes to a Corpus have been made:
Two Corpora with the same name tag and date tag **must not** contradict
  each other.
This is easy to ensure if one always gives a Corpus a domain one
  controls (ideally, the domain it is hosted at) and simply updates the
  date after every backwards‐incompatible change.
In general, one should strive **not** to introduce
  backwards‐incompatible changes, and maintain Corpora with their
  existing name tags and date tags while expanding their contents.

Corpora describe the works of a single Pseud, who may not have any
  relation to the person managing the Corpus or to the domain signified
  in the name tag.
Pseuds are an abstraction for a notion of authorship which is
  intentionally divorced from real individuals:
A Pseud could indicate any number of people, and people may be
  represented by any number of Pseuds.
In general, the concept of Pseud exists to answer the question of “why
  are these works being aggregated together?”, with the answer being
  that they have some shared particularities of authorship which the
  Pseud stands to represent.

The relationship between Pseuds and works collected in a Corpus need
  not be exhaustive:
Works may have other authors or provenances provided they broadly fall
  under the umbrella of a [Project](#Project) that the Pseud played a
  significant role in.

### <span class="numero">2.2</span> Projects and Branches
{:id="overview.branches"}

<div style="Text-Align: Center"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" style="background:#FFFFFF;" version="1.1" viewBox="0 0 1241 567"><defs/><g xmlns="http://www.w3.org/2000/svg"><g id="elem_Corpus"><rect fill="#F1F1F1" height="81.1875" style="stroke:#181818;stroke-width:0.5;" width="231" x="54" y="134.5"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="50" x="144.5" y="149.4951">Corpus</text><line style="stroke:#181818;stroke-width:1.0;" x1="54" x2="285" y1="154.7969" y2="154.7969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="57" x="64.5" y="169.792">full title</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="123" x="137" y="169.792"> "My Corpus"@en</text><line style="stroke:#181818;stroke-width:1.0;" x1="132" x2="132" y1="154.7969" y2="175.0938"/><line style="stroke:#181818;stroke-width:1.0;" x1="54" x2="285" y1="175.0938" y2="175.0938"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="61" x="62.5" y="190.0889">date tag</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="95" x="137" y="190.0889">"2023-07-23"</text><line style="stroke:#181818;stroke-width:1.0;" x1="132" x2="132" y1="175.0938" y2="195.3906"/><line style="stroke:#181818;stroke-width:1.0;" x1="54" x2="285" y1="195.3906" y2="195.3906"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="68" x="59" y="210.3857">name tag</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="143" x="137" y="210.3857">"bns.example.com"</text><line style="stroke:#181818;stroke-width:1.0;" x1="132" x2="132" y1="195.3906" y2="215.6875"/></g><g id="elem_Project"><rect fill="#F1F1F1" height="81.1875" style="stroke:#181818;stroke-width:0.5;" width="209" x="504" y="8.5"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="52" x="582.5" y="23.4951">Project</text><line style="stroke:#181818;stroke-width:1.0;" x1="504" x2="713" y1="28.7969" y2="28.7969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="57" x="514" y="43.792">full title</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="122" x="586" y="43.792">"My Project"@en</text><line style="stroke:#181818;stroke-width:1.0;" x1="581" x2="581" y1="28.7969" y2="49.0938"/><line style="stroke:#181818;stroke-width:1.0;" x1="504" x2="713" y1="49.0938" y2="49.0938"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="39" x="523" y="64.0889">index</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="12" x="586" y="64.0889"> 1</text><line style="stroke:#181818;stroke-width:1.0;" x1="581" x2="581" y1="49.0938" y2="69.3906"/><line style="stroke:#181818;stroke-width:1.0;" x1="504" x2="713" y1="69.3906" y2="69.3906"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="67" x="509" y="84.3857">identifier</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="96" x="586" y="84.3857">"my_project"</text><line style="stroke:#181818;stroke-width:1.0;" x1="581" x2="581" y1="69.3906" y2="89.6875"/></g><g id="elem_Project2"><rect fill="#F1F1F1" height="81.1875" style="stroke:#181818;stroke-width:0.5;" width="255" x="481" y="256.5"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="52" x="582.5" y="271.4951">Project</text><line style="stroke:#181818;stroke-width:1.0;" x1="481" x2="736" y1="276.7969" y2="276.7969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="57" x="491" y="291.792">full title</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="168" x="563" y="291.792">"My Other Project"@en</text><line style="stroke:#181818;stroke-width:1.0;" x1="558" x2="558" y1="276.7969" y2="297.0938"/><line style="stroke:#181818;stroke-width:1.0;" x1="481" x2="736" y1="297.0938" y2="297.0938"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="39" x="500" y="312.0889">index</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="11" x="563" y="312.0889"> 2</text><line style="stroke:#181818;stroke-width:1.0;" x1="558" x2="558" y1="297.0938" y2="317.3906"/><line style="stroke:#181818;stroke-width:1.0;" x1="481" x2="736" y1="317.3906" y2="317.3906"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="67" x="486" y="332.3857">identifier</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="75" x="563" y="332.3857">"project2"</text><line style="stroke:#181818;stroke-width:1.0;" x1="558" x2="558" y1="317.3906" y2="337.6875"/></g><g id="elem_Book"><rect fill="#F1F1F1" height="60.8906" style="stroke:#181818;stroke-width:0.5;" width="225" x="496" y="149"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="37" x="590" y="163.9951">Book</text><line style="stroke:#181818;stroke-width:1.0;" x1="496" x2="721" y1="169.2969" y2="169.2969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="57" x="501" y="184.292">full title</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="148" x="568" y="184.292">"Book the First"@en</text><line style="stroke:#181818;stroke-width:1.0;" x1="563" x2="563" y1="169.2969" y2="189.5938"/><line style="stroke:#181818;stroke-width:1.0;" x1="496" x2="721" y1="189.5938" y2="189.5938"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="39" x="510" y="204.5889">index</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="9" x="568" y="204.5889">1</text><line style="stroke:#181818;stroke-width:1.0;" x1="563" x2="563" y1="189.5938" y2="209.8906"/></g><g id="elem_Concept1"><rect fill="#F1F1F1" height="60.8906" style="stroke:#181818;stroke-width:0.5;" width="229" x="958" y="90"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="62" x="1041.5" y="104.9951">Concept</text><line style="stroke:#181818;stroke-width:1.0;" x1="958" x2="1187" y1="110.2969" y2="110.2969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="57" x="963" y="125.292">full title</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="152" x="1030" y="125.292">"Initial Concept"@en</text><line style="stroke:#181818;stroke-width:1.0;" x1="1025" x2="1025" y1="110.2969" y2="130.5938"/><line style="stroke:#181818;stroke-width:1.0;" x1="958" x2="1187" y1="130.5938" y2="130.5938"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="39" x="972" y="145.5889">index</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="9" x="1030" y="145.5889">1</text><line style="stroke:#181818;stroke-width:1.0;" x1="1025" x2="1025" y1="130.5938" y2="150.8906"/></g><g id="elem_Concept2"><rect fill="#F1F1F1" height="60.8906" style="stroke:#181818;stroke-width:0.5;" width="179" x="983" y="193"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="62" x="1041.5" y="207.9951">Concept</text><line style="stroke:#181818;stroke-width:1.0;" x1="983" x2="1162" y1="213.2969" y2="213.2969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="57" x="988" y="228.292">full title</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="102" x="1055" y="228.292">"Revised"@en</text><line style="stroke:#181818;stroke-width:1.0;" x1="1050" x2="1050" y1="213.2969" y2="233.5938"/><line style="stroke:#181818;stroke-width:1.0;" x1="983" x2="1162" y1="233.5938" y2="233.5938"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="39" x="997" y="248.5889">index</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="8" x="1055" y="248.5889">2</text><line style="stroke:#181818;stroke-width:1.0;" x1="1050" x2="1050" y1="233.5938" y2="253.8906"/></g><g id="elem_Book21"><rect fill="#F1F1F1" height="60.8906" style="stroke:#181818;stroke-width:0.5;" width="193" x="512" y="500"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="37" x="590" y="514.9951">Book</text><line style="stroke:#181818;stroke-width:1.0;" x1="512" x2="705" y1="520.2969" y2="520.2969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="57" x="517" y="535.292">full title</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="116" x="584" y="535.292">"Book One"@en</text><line style="stroke:#181818;stroke-width:1.0;" x1="579" x2="579" y1="520.2969" y2="540.5938"/><line style="stroke:#181818;stroke-width:1.0;" x1="512" x2="705" y1="540.5938" y2="540.5938"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="39" x="526" y="555.5889">index</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="9" x="584" y="555.5889">1</text><line style="stroke:#181818;stroke-width:1.0;" x1="579" x2="579" y1="540.5938" y2="560.8906"/></g><g id="elem_Book22"><rect fill="#F1F1F1" height="60.8906" style="stroke:#181818;stroke-width:0.5;" width="193" x="512" y="397"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="37" x="590" y="411.9951">Book</text><line style="stroke:#181818;stroke-width:1.0;" x1="512" x2="705" y1="417.2969" y2="417.2969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="57" x="517" y="432.292">full title</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="116" x="584" y="432.292">"Book Two"@en</text><line style="stroke:#181818;stroke-width:1.0;" x1="579" x2="579" y1="417.2969" y2="437.5938"/><line style="stroke:#181818;stroke-width:1.0;" x1="512" x2="705" y1="437.5938" y2="437.5938"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="39" x="526" y="452.5889">index</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="8" x="584" y="452.5889">2</text><line style="stroke:#181818;stroke-width:1.0;" x1="579" x2="579" y1="437.5938" y2="457.8906"/></g><g id="elem_Concept21"><rect fill="#F1F1F1" height="60.8906" style="stroke:#181818;stroke-width:0.5;" width="223" x="961" y="500"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="62" x="1041.5" y="514.9951">Concept</text><line style="stroke:#181818;stroke-width:1.0;" x1="961" x2="1184" y1="520.2969" y2="520.2969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="57" x="966" y="535.292">full title</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="146" x="1033" y="535.292">"First Attempt"@en</text><line style="stroke:#181818;stroke-width:1.0;" x1="1028" x2="1028" y1="520.2969" y2="540.5938"/><line style="stroke:#181818;stroke-width:1.0;" x1="961" x2="1184" y1="540.5938" y2="540.5938"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="39" x="975" y="555.5889">index</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="9" x="1033" y="555.5889">1</text><line style="stroke:#181818;stroke-width:1.0;" x1="1028" x2="1028" y1="540.5938" y2="560.8906"/></g><g id="elem_Concept22"><rect fill="#F1F1F1" height="60.8906" style="stroke:#181818;stroke-width:0.5;" width="223" x="961" y="397"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="62" x="1041.5" y="411.9951">Concept</text><line style="stroke:#181818;stroke-width:1.0;" x1="961" x2="1184" y1="417.2969" y2="417.2969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="57" x="966" y="432.292">full title</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="146" x="1033" y="432.292">"First Attempt"@en</text><line style="stroke:#181818;stroke-width:1.0;" x1="1028" x2="1028" y1="417.2969" y2="437.5938"/><line style="stroke:#181818;stroke-width:1.0;" x1="961" x2="1184" y1="437.5938" y2="437.5938"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="39" x="975" y="452.5889">index</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="9" x="1033" y="452.5889">1</text><line style="stroke:#181818;stroke-width:1.0;" x1="1028" x2="1028" y1="437.5938" y2="457.8906"/></g><g id="elem_Description"><rect fill="#F1F1F1" height="40.5938" style="stroke:#181818;stroke-width:0.5;" width="325" x="7" y="275"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="84" x="127.5" y="289.9951">Description</text><line style="stroke:#181818;stroke-width:1.0;" x1="7" x2="332" y1="295.2969" y2="295.2969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="65" x="12" y="310.292">contents</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="240" x="87" y="310.292">"My amazing corpus, by me."@en</text><line style="stroke:#181818;stroke-width:1.0;" x1="82" x2="82" y1="295.2969" y2="315.5938"/></g><g id="elem_PrjDesc"><rect fill="#F1F1F1" height="40.5938" style="stroke:#181818;stroke-width:0.5;" width="282" x="931.5" y="7"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="84" x="1030.5" y="21.9951">Description</text><line style="stroke:#181818;stroke-width:1.0;" x1="931.5" x2="1213.5" y1="27.2969" y2="27.2969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="65" x="936.5" y="42.292">contents</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="197" x="1011.5" y="42.292">"My very first project."@en</text><line style="stroke:#181818;stroke-width:1.0;" x1="1006.5" x2="1006.5" y1="27.2969" y2="47.5938"/></g><g id="elem_Prj2Desc"><rect fill="#F1F1F1" height="40.5938" style="stroke:#181818;stroke-width:0.5;" width="323" x="911" y="296"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="84" x="1030.5" y="310.9951">Description</text><line style="stroke:#181818;stroke-width:1.0;" x1="911" x2="1234" y1="316.2969" y2="316.2969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="65" x="916" y="331.292">contents</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="238" x="991" y="331.292">"A totally different project."@en</text><line style="stroke:#181818;stroke-width:1.0;" x1="986" x2="986" y1="316.2969" y2="336.5938"/></g><!--link Corpus to Project--><g id="link_Corpus_Project"><path codeLine="54" d="M293.101,139.651 C354.985,121.808 429.672,100.273 490.721,82.671 " fill="none" id="Corpus-to-Project" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="495.837,81.196,486.0814,79.8442,491.0325,82.5804,488.2963,87.5315,495.837,81.196" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="71" x="371" y="94.0669">has project</text></g><!--link Corpus to Project2--><g id="link_Corpus_Project2"><path codeLine="55" d="M293.101,209.227 C347.386,224.382 411.522,242.288 467.663,257.961 " fill="none" id="Corpus-to-Project2" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="472.724,259.374,465.1309,253.1015,467.9081,258.0297,462.9799,260.8069,472.724,259.374" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="71" x="371" y="229.0669">has project</text></g><!--link Corpus to Description--><g id="link_Corpus_Description"><path codeLine="56" d="M169.5,219.636 C169.5,234.907 169.5,251.603 169.5,265.39 " fill="none" id="Corpus-to-Description" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="169.5,270.716,173.5,261.716,169.5,265.716,165.5,261.716,169.5,270.716" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="97" x="109.5" y="249.8169">has description</text></g><!--link Project to Book--><g id="link_Project_Book"><path codeLine="57" d="M608.5,93.693 C608.5,108.612 608.5,125.119 608.5,139.605 " fill="none" id="Project-to-Book" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="608.5,144.844,612.5,135.844,608.5,139.844,604.5,135.844,608.5,144.844" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="52" x="571" y="123.8169">includes</text></g><!--link Project to PrjDesc--><g id="link_Project_PrjDesc"><path codeLine="58" d="M721.043,43.688 C780.249,40.869 853.9,37.362 918.381,34.291 " fill="none" id="Project-to-PrjDesc" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="923.401,34.052,914.2214,30.4834,918.4066,34.2891,914.6008,38.4744,923.401,34.052" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="97" x="775" y="33.0669">has description</text></g><!--link Book to Concept1--><g id="link_Book_Concept1"><path codeLine="59" d="M729.077,163.729 C795.132,155.294 877.172,144.817 944.388,136.233 " fill="none" id="Book-to-Concept1" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="949.61,135.566,940.176,132.7376,944.6502,136.199,941.1888,140.6732,949.61,135.566" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="52" x="797.5" y="142.0669">includes</text></g><!--link Book to Concept2--><g id="link_Book_Concept2"><path codeLine="60" d="M729.077,190.388 C803.75,197.5 898.85,206.557 969.871,213.321 " fill="none" id="Book-to-Concept2" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="974.942,213.804,966.3607,208.9707,969.9644,213.3311,965.604,216.9349,974.942,213.804" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="52" x="797.5" y="192.0669">includes</text></g><!--link Project2 to Book21--><g id="link_Project2_Book21"><path codeLine="61" d="M472.775,335.526 C427.79,356.301 393.469,386.307 412.5,427.5 C428.954,463.113 463.49,486.6263 499.056,502.0247 " fill="none" id="Project2-to-Book21" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="503.851,504.0501,497.1169,496.8632,499.2451,502.1044,494.0038,504.2326,503.851,504.0501" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="52" x="380.5" y="423.5669">includes</text></g><!--link Project2 to Book22--><g id="link_Project2_Book22"><path codeLine="62" d="M608.5,341.693 C608.5,356.612 608.5,373.119 608.5,387.605 " fill="none" id="Project2-to-Book22" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="608.5,392.844,612.5,383.844,608.5,387.844,604.5,383.844,608.5,392.844" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="52" x="571" y="371.8169">includes</text></g><!--link Project2 to Prj2Desc--><g id="link_Project2_Prj2Desc"><path codeLine="63" d="M744.061,302.534 C792.091,304.509 846.901,306.763 897.759,308.855 " fill="none" id="Project2-to-Prj2Desc" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="902.984,309.07,894.1549,304.7056,897.9882,308.8657,893.8281,312.6989,902.984,309.07" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="97" x="775" y="301.0669">has description</text></g><!--link Book21 to Concept21--><g id="link_Book21_Concept21"><path codeLine="64" d="M713.182,530 C782.55,530 874.194,530 947.435,530 " fill="none" id="Book21-to-Concept21" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="952.676,530,943.676,526,947.676,530,943.676,534,952.676,530" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="52" x="797.5" y="526.0669">includes</text></g><!--link Book22 to Concept22--><g id="link_Book22_Concept22"><path codeLine="65" d="M713.182,427 C782.55,427 874.194,427 947.435,427 " fill="none" id="Book22-to-Concept22" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="952.676,427,943.676,423,947.676,427,943.676,431,952.676,427" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="52" x="797.5" y="423.0669">includes</text></g><!--SRC=[dPJDRjGm58NtUOghh9bHSxj0giHeIX4I2rI4s2CtkPbnyKzatvG6n88duTru4XAFhPbE753PPNpE_SwnPS_Mit3SQrKeQXdOWfExFKCZ7TKihIcqwE36kgxtyBq0Q7kbW2KhWbURtpz_bU-7A9TRCgEX4Kp0OZVAKE85NgukNgxmiXmb8_HHkZD-JOz2TuhMjTLbyID4VN3sVaoUPy6ObDGO9Kr3ZuSQrVIp8SEobUI2NG-VksYFx3ADW_09XGj9jxmdjvo7yxmOXYdjjRLVSl2mFjBXhNJ-70rLwdfZJKtTeMlyhdAuTqQo52fwdiNBxlqZFKXFpJa78oVK3v3f0wl57TuQodJ9CN2HyUchpVV8xoVV9XmpN3EJVkRnu7_Ztf2ldUoc1nGOjJLCXdswNKABRzBie0ulvmNS3Q1f7MYnpmaXNjVxQIbBUo0tG1jwnKluBnJcMTV0beLI0pIoRScHcH7ZNy5gjKcFumhsmYTRHiUyONDwI0TBSrmeul3e2vVa2gIfLJywaX8YugdCnwUPu4XFvmYOQJY7Oyh5wczYTZBsmkLYM3s9F--6U2e_2TcIQNgj_W00]--></g></svg></div>

Each broad endeavour in a [Corpus](#Corpus) is termed a
  [Project](#Project).
Projects **must** have both an integral, non·negative [index](#index)
  and an N·C·Name [identifier](#identifier), neither of which is
  allowed to change after project creation.
Typically, projects are ordered in rough order of creation, starting
  from 001.

In the creative arts, a Project corresponds roughly with the idea of a
  “universe”, in that it may contain many different works across many
  different mediums, but generally speaking there are shared concepts,
  events, or esthetic qualities between the works which allow them to
  be treated as belonging together.
The <cite>B·N·S</cite> cannot accommodate a work existing in multiple
  Projects at once, so the person managing the Corpus will have to make
  some decisions in cases of cross·over.

Projects, in turn, are broken up across several [Branches](#Branch),
  which they [include](#includes) and which are included [in](#in) each
  other.
Branches come in two varieties, [Division](#Division) and
  [Revision](#Revision).
Divisions are used to divide a work up into smaller parts, and are
  often optional.
Revisions, on the other hand, indicate alternate expressions of *the
  same* portion of a work, and are fundamental to its branch structure.
The available Divisions and Revisions are summarized in the table
  below :&#x2060;—

| Branch Name | Branch Kind | Abbreviated Prefix | Numeric Format |
| :---------: | :---------: | :----------------: | :------------: |
| [Book](#Book) | Division | `Bbl:` | Α, Β, Γ, Δ… |
| **[Concept](#Concept)** | **Revision** | `Ccp:` | 1°, 2°, 3°, 4°… |
| [Volume](#Volume) | Division | `Vol:` | Ⅰ, ⅠⅠ, ⅠⅠⅠ, ⅠⅤ… |
| [Arc](#Arc) | Division | `Arc:` | α, β, γ, δ… |
| **[Version](#Version)** | **Revision** | `Vsn:` | 1′, 2′, 3′, 4′… |
| [Side](#Side) | Division | `Vol:` | A, B, C, D… |
| [Chapter](#Chapter) | Division | `Chp:` | 01, 02, 03, 04… |
| **[Draft](#Draft)** | **Revision** | `Dft:` | 1″, 2″, 3″, 4″… |
| [Section](#Section) | Division | `Sec:` | a, b, c, d… |
| [Verse](#Verse) | Division | `Vrs:` | ⅰ, ⅰⅰ, ⅰⅰⅰ, ⅰⅴ… |

Projects themselves **must** include Books, which signify a single work
  of potentially multiple parts.
From there, each branch may include any branch beneath it in the table,
  *up to the next Revision*.
Books can only include Concepts, but Concepts can include Volumes,
  Arcs, or Versions (but not Sides, Chapters, or Drafts).
This allows Divisions to be “skipped” if the work in question does not
  make use of them.
Revisions **must not** be skipped, as that would make them impossible
  to re·introduce later.

There is another restriction in addition to the above:
Within a Revision, the branching structure **must be kept consistent,
  _up to the next Revision descendant_**, so (for example) if a Version
  includes a Side which includes a Chapter, *all* of the Sides it
  includes must include Chapters.
Another way of expressing this requirement is to say that whenever the
  branch structure changes, it is necessary to introduce a new Revision
  which encapsulates the change.

Like projects, each Branch has an index.
Branch indices may be negative, or in place of an integer they may be
  an N·C·Name, a year~month (`YYYY-MM`), or a date (`YYYY-MM-DD`).
(Plain years are not allowed as they would be, in many cases,
  indistinguishable from integers.)
When the index is a positive integer, it is often formatted specially.
This allows for a concise description of an Branch’s location within
  a corpus; for example, `008:Γ:2°:ⅤⅠⅤ:δ:1′:E:06:3″:g:ⅰⅴ` indicates the
  fourth Verse of the seventh Section of the third Draft of the sixth
  Chapter of the fifth Side of the first Version of the fourth Arc of
  the ninth Volume of the second Concept of the third Book of the
  eighth Project of some Corpus.
Note that logically, “Side E” still has an index of `5` (not `"E"`,
  which would instead be formatted as “Side 'E”).

Each branch type also has an associated prefix, which can be used in
  the construction of file paths or [Tag U·R·I][RFC4151]’s.
For example,
  <code>tag:bns.example,2000:B@N/<wbr/>Prj:8/<wbr/>Bbl:3/<wbr/>Ccp:2/<wbr/>Vol:9/<wbr/>Arc:4/<wbr/>Vsn:1/<wbr/>Sde:5/<wbr/>Chp:3/<wbr/>Dft:3/<wbr/>Sec:7/<wbr/>Vrs:4</code>.

### <span class="numero">2.3</span> Attaching Files
{:id="overview.files"}

<div style="Text-Align: Center"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" style="background:#FFFFFF;" version="1.1" viewBox="0 0 778 277"><defs/><g xmlns="http://www.w3.org/2000/svg"><g id="elem_Draft"><rect fill="#F1F1F1" height="40.5938" style="stroke:#181818;stroke-width:0.5;" width="71" x="288.8841" y="7"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="38" x="305.3841" y="21.9951">Draft</text><line style="stroke:#181818;stroke-width:1.0;" x1="288.8841" x2="359.8841" y1="27.2969" y2="27.2969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="39" x="293.8841" y="42.292">index</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="12" x="342.8841" y="42.292"> 3</text><line style="stroke:#181818;stroke-width:1.0;" x1="337.8841" x2="337.8841" y1="27.2969" y2="47.5938"/></g><g id="elem_File"><rect fill="#F1F1F1" height="60.8906" style="stroke:#181818;stroke-width:0.5;" width="227" x="210.8841" y="210"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="208" x="220.3841" y="224.9951">File &lt;/c1v2d3/files/text.pdf&gt;</text><line style="stroke:#181818;stroke-width:1.0;" x1="210.8841" x2="437.8841" y1="230.2969" y2="230.2969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="34" x="239.8841" y="245.292">type</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="33" x="307.8841" y="245.292">Text</text><line style="stroke:#181818;stroke-width:1.0;" x1="302.8841" x2="302.8841" y1="230.2969" y2="250.5938"/><line style="stroke:#181818;stroke-width:1.0;" x1="210.8841" x2="437.8841" y1="250.5938" y2="250.5938"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="82" x="215.8841" y="265.5889">media type</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="125" x="307.8841" y="265.5889">"application/pdf"</text><line style="stroke:#181818;stroke-width:1.0;" x1="302.8841" x2="302.8841" y1="250.5938" y2="270.8906"/></g><g id="elem_FileSequence"><rect fill="#F1F1F1" height="60.8906" style="stroke:#181818;stroke-width:0.5;" width="213" x="217.8841" y="107"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="99" x="274.8841" y="121.9951">File Sequence</text><line style="stroke:#181818;stroke-width:1.0;" x1="217.8841" x2="430.8841" y1="127.2969" y2="127.2969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="34" x="246.8841" y="142.292">type</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="111" x="314.8841" y="142.292">Text, Sequence</text><line style="stroke:#181818;stroke-width:1.0;" x1="309.8841" x2="309.8841" y1="127.2969" y2="147.5938"/><line style="stroke:#181818;stroke-width:1.0;" x1="217.8841" x2="430.8841" y1="147.5938" y2="147.5938"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="82" x="222.8841" y="162.5889">media type</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="91" x="314.8841" y="162.5889">"image/png"</text><line style="stroke:#181818;stroke-width:1.0;" x1="309.8841" x2="309.8841" y1="147.5938" y2="167.8906"/></g><g id="elem_Page01"><rect fill="#F1F1F1" height="20.2969" style="stroke:#181818;stroke-width:0.5;" width="219" x="553.8841" y="56"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="205" x="560.8841" y="70.9951">&lt;/c1v2d3/files/page01.png&gt;</text></g><g id="elem_Page02"><rect fill="#F1F1F1" height="20.2969" style="stroke:#181818;stroke-width:0.5;" width="218" x="554.3841" y="127"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="204" x="561.3841" y="141.9951">&lt;/c1v2d3/files/page02.png&gt;</text></g><g id="elem_Page03"><rect fill="#F1F1F1" height="20.2969" style="stroke:#181818;stroke-width:0.5;" width="219" x="553.8841" y="198"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="205" x="560.8841" y="212.9951">&lt;/c1v2d3/files/page03.png&gt;</text></g><!--link Draft to File--><g id="link_Draft_File"><path codeLine="19" d="M280.7951,27.374 C217.9231,30.557 103.5631,46.819 50.8841,120.5 C6,183.2788 105.9101,213.4575 197.3311,227.7198 " fill="none" id="Draft-to-File" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="202.5061,228.5117,194.2156,223.195,197.5638,227.7546,193.0042,231.1027,202.5061,228.5117" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="158" x="13.8841" y="133.5669">has digital manifestation</text></g><!--link Draft to FileSequence--><g id="link_Draft_FileSequence"><path codeLine="20" d="M324.3841,51.061 C324.3841,64.679 324.3841,82.048 324.3841,97.601 " fill="none" id="Draft-to-FileSequence" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="324.3841,102.798,328.3841,93.798,324.3841,97.798,320.3841,93.798,324.3841,102.798" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="158" x="233.8841" y="81.5669">has digital manifestation</text></g><!--link FileSequence to Page01--><g id="link_FileSequence_Page01"><path codeLine="21" d="M439.0071,113.062 C482.3411,103.932 531.1111,93.657 572.0001,85.042 " fill="none" id="FileSequence-to-Page01" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="576.9461,84,567.3144,81.943,572.0537,85.0316,568.9651,89.7709,576.9461,84" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="38" x="476.8841" y="94.0669">rdf:_1</text></g><!--link FileSequence to Page02--><g id="link_FileSequence_Page02"><path codeLine="22" d="M439.0071,137 C471.8371,137 507.7871,137 541.0661,137 " fill="none" id="FileSequence-to-Page02" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="546.3831,137,537.3831,133,541.3831,137,537.3831,141,546.3831,137" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="38" x="476.8841" y="133.0669">rdf:_2</text></g><!--link FileSequence to Page03--><g id="link_FileSequence_Page03"><path codeLine="23" d="M439.0071,160.938 C482.3411,170.068 531.1111,180.3431 572.0001,188.9577 " fill="none" id="FileSequence-to-Page03" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="576.9461,189.9998,568.9648,184.2293,572.0536,188.9684,567.3146,192.0573,576.9461,189.9998" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="38" x="476.8841" y="167.0669">rdf:_3</text></g><!--SRC=[VP3FJeGm4CRlFCM4ion2RqJ97end4xsRXaxPITfkXS6isNZmINmtdyHk8IImMMwTVhzl_dpxdcN7WpM9GSt0H-YeFJ0ewh1XEhh4IW_fKoSrfo1xY2yu9m3a59xWiVxx-HN9zyWzaq5uo9lYirGYrw7gSyOJxxpITVH7ucBdBu_13Mz13gL5HNB-JANtXXfvsI0FtdGnu1K_1dGDpWsdZ-l6TpDxFOAiR37thfsRhnRtGRylTe4OLt-9DPoty7A5bzkuME4YucF6MHrlg-0GL4KjiJHWfIEDFSTabk1qwQPX4LQMrTDD5NHAL-_53RsSzFA6BYPT97jqQh3c7m00]--></g></svg></div>

Some [Branches](#Branch) [have digital
  manifestations](#hasDigitalManifestation), which can be either
  [Files](#File) or [File Sequences](#FileSequence).
File Sequences are provided for the case in which the [Digital
  Manifestation](#DigitalManifestation) is paginated across multiple
  files of the same type, for example a comic which is represented as
  multiple images.
There is no way to represent complex file trees in the
  <cite>B·N·S</cite> vocabulary; if files are not all of the same type,
  they must first be encoded in some kind of container format (like a
  Zip archive).

Every Digital Manifestation must have a [media type](#mediaType) from
  the [I·A·N·A media types registry][Media-Types], which indicates the
  kind of file(s) it represents.
It is recommended that they also be assigned a type from [the
  <cite>D·C·M·I Type</cite> vocabulary][DCMI], for example
  [Text](#dcmitype:Text).
Finally, if the Digital Manifestation is a File Sequence, it should be
  identified as either a [Bag](#rdf:Bag) or [Sequence](#rdf:Seq),
  depending on whether it is unordered or ordered.

It is expected that fetching the I·R·I of a File, or of a File
  Sequence’s [members](#rdfs:member), will yield a binary
  representation corresponding to its media type.
As each Digital Manifestation can be associated with exactly one thing,
  this means that these I·R·I’s **should** be Branch‐specific.
Symbolic links or other mechanisms may be used in the case where the
  same binary file needs to appear in the Digital Manifestation of
  multiple Branches.

Only Branches which cannot [include](#includes) further
  [Revisions](#Revision) can have digital manifestations.
This limits them to [Drafts](#Draft), [Sections](#Section), and
  [Verses](#Verse).
Typically, if a Branch has a digital manifestation, it does not branch
  further.

### <span class="numero">2.4</span> Useful Metadata
{:id="overview.metadata"}

Regardless of whether a thing is a [Pseud](#Pseud), a
  [Corpus](#Corpus), a [Project](#Project), a [Branch](#Branch), or
  indeed anything else, it is good to provide it with a [full
  title](#fullTitle).
In space‐constrained environments, sometimes the full title is not
  entirely necessary, and a [short title](#shortTitle) can be provided
  for these situations.

Resources can also [have a description](#hasDescription).
[Descriptions](#Description) are modelled in the <cite>B·N·S</cite>
  vocabulary as [Content Resources](#ContentResource), whose
  [contents](#contents) provide the actual description.
Contents can be provided as a string or as X·M·L; in the latter case,
  it is recommended to use the `@xml:lang` attribute to specify the
  language of the contents.

### <span class="numero">2.5</span> Complementary Resources
{:id="overview.complementary"}

<div style="Text-Align: Center"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" style="background:#FFFFFF;" version="1.1" viewBox="0 0 658 355"><defs/><g xmlns="http://www.w3.org/2000/svg"><g id="elem_Version"><rect fill="#F1F1F1" height="40.5938" style="stroke:#181818;stroke-width:0.5;" width="70" x="78.5" y="17"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="54" x="86.5" y="31.9951">Version</text><line style="stroke:#181818;stroke-width:1.0;" x1="78.5" x2="148.5" y1="37.2969" y2="37.2969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="39" x="83.5" y="52.292">index</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="11" x="132.5" y="52.292"> 2</text><line style="stroke:#181818;stroke-width:1.0;" x1="127.5" x2="127.5" y1="37.2969" y2="57.5938"/></g><g id="elem_Note"><rect fill="#F1F1F1" height="60.8906" style="stroke:#181818;stroke-width:0.5;" width="192" x="17.5" y="117"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="35" x="96" y="131.9951">Note</text><line style="stroke:#181818;stroke-width:1.0;" x1="17.5" x2="209.5" y1="137.2969" y2="137.2969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="67" x="22.5" y="152.292">identifier</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="63" x="99.5" y="152.292">my_note</text><line style="stroke:#181818;stroke-width:1.0;" x1="94.5" x2="94.5" y1="137.2969" y2="157.5938"/><line style="stroke:#181818;stroke-width:1.0;" x1="17.5" x2="209.5" y1="157.5938" y2="157.5938"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="57" x="27.5" y="172.5889">full title</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="105" x="99.5" y="172.5889">"My Note"@en</text><line style="stroke:#181818;stroke-width:1.0;" x1="94.5" x2="94.5" y1="157.5938" y2="177.8906"/></g><g id="elem_FileSequence"><rect fill="#F1F1F1" height="60.8906" style="stroke:#181818;stroke-width:0.5;" width="213" x="7" y="237"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="99" x="64" y="251.9951">File Sequence</text><line style="stroke:#181818;stroke-width:1.0;" x1="7" x2="220" y1="257.2969" y2="257.2969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="34" x="36" y="272.292">type</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="111" x="104" y="272.292">Text, Sequence</text><line style="stroke:#181818;stroke-width:1.0;" x1="99" x2="99" y1="257.2969" y2="277.5938"/><line style="stroke:#181818;stroke-width:1.0;" x1="7" x2="220" y1="277.5938" y2="277.5938"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="82" x="12" y="292.5889">media type</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="91" x="104" y="292.5889">"image/png"</text><line style="stroke:#181818;stroke-width:1.0;" x1="99" x2="99" y1="277.5938" y2="297.8906"/></g><g id="elem_Cover"><rect fill="#F1F1F1" height="60.8906" style="stroke:#181818;stroke-width:0.5;" width="290" x="361" y="7"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="42" x="485" y="21.9951">Cover</text><line style="stroke:#181818;stroke-width:1.0;" x1="361" x2="651" y1="27.2969" y2="27.2969"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="34" x="381.5" y="42.292">type</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="130" x="441" y="42.292">Content Resource</text><line style="stroke:#181818;stroke-width:1.0;" x1="436" x2="436" y1="27.2969" y2="47.5938"/><line style="stroke:#181818;stroke-width:1.0;" x1="361" x2="651" y1="47.5938" y2="47.5938"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="65" x="366" y="62.5889">contents</text><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="205" x="441" y="62.5889">"&lt;svg …&gt;"^^rdf:XMLLiteral</text><line style="stroke:#181818;stroke-width:1.0;" x1="436" x2="436" y1="47.5938" y2="67.8906"/></g><g id="elem_Page01"><rect fill="#F1F1F1" height="20.2969" style="stroke:#181818;stroke-width:0.5;" width="285" x="363.5" y="186"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="271" x="370.5" y="200.9951">&lt;/c1v2/:notes/my_note/page01.png&gt;</text></g><g id="elem_Page02"><rect fill="#F1F1F1" height="20.2969" style="stroke:#181818;stroke-width:0.5;" width="284" x="364" y="257"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="270" x="371" y="271.9951">&lt;/c1v2/:notes/my_note/page02.png&gt;</text></g><g id="elem_Page03"><rect fill="#F1F1F1" height="20.2969" style="stroke:#181818;stroke-width:0.5;" width="285" x="363.5" y="328"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacing" textLength="271" x="370.5" y="342.9951">&lt;/c1v2/:notes/my_note/page03.png&gt;</text></g><!--link Version to Note--><g id="link_Version_Note"><path codeLine="23" d="M113.5,61.061 C113.5,74.679 113.5,92.048 113.5,107.601 " fill="none" id="Version-to-Note" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="113.5,112.798,117.5,103.798,113.5,107.798,109.5,103.798,113.5,112.798" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="56" x="74" y="91.5669">has note</text></g><!--link Version to Cover--><g id="link_Version_Cover"><path codeLine="24" d="M156.661,37 C202.407,37 278.199,37 347.423,37 " fill="none" id="Version-to-Cover" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="352.819,37,343.819,33,347.819,37,343.819,41,352.819,37" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="63" x="259" y="33.0669">has cover</text></g><!--link Note to FileSequence--><g id="link_Note_FileSequence"><path codeLine="25" d="M113.5,181.151 C113.5,195.729 113.5,212.823 113.5,227.903 " fill="none" id="Note-to-FileSequence" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="113.5,232.934,117.5,223.934,113.5,227.934,109.5,223.934,113.5,232.934" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="158" x="23" y="211.5669">has digital manifestation</text></g><!--link FileSequence to Page01--><g id="link_FileSequence_Page01"><path codeLine="26" d="M228.236,246.321 C282.765,236.406 347.634,224.612 400.859,214.935 " fill="none" id="FileSequence-to-Page01" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="405.972,214.005,396.4016,211.6795,401.0527,214.8994,397.8327,219.5504,405.972,214.005" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="38" x="271.5" y="225.0669">rdf:_1</text></g><!--link FileSequence to Page02--><g id="link_FileSequence_Page02"><path codeLine="27" d="M228.236,267 C266.376,267 309.573,267 350.307,267 " fill="none" id="FileSequence-to-Page02" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="355.778,267,346.778,263,350.778,267,346.778,271,355.778,267" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="38" x="271.5" y="263.0669">rdf:_2</text></g><!--link FileSequence to Page03--><g id="link_FileSequence_Page03"><path codeLine="28" d="M228.236,287.6792 C282.765,297.5937 347.634,309.3879 400.859,319.0652 " fill="none" id="FileSequence-to-Page03" style="stroke:#181818;stroke-width:1.0;"/><polygon fill="#181818" points="405.972,319.995,397.8326,314.4498,401.0526,319.1007,396.4017,322.3208,405.972,319.995" style="stroke:#181818;stroke-width:1.0;"/><text fill="#000000" font-family="sans-serif" font-size="13" lengthAdjust="spacing" textLength="38" x="271.5" y="290.0669">rdf:_3</text></g><!--SRC=[ZPB1IiD048RlUOeXPpKskGKD1S5JAw8YdbgMP98kR3PrTr9QHD0dyUQ3zKcStKrhAHIzJURx_t-OoOuj2KDzgoA5DG5rO6Ip8AYamP9afwDMB25-HcFv8mPXOQZXBGAGki8rN1VRpwyqUW_QkuxG2rqHL1LgahL4mr9eDtFDXFjrhnIG98Mk7qytuBrZrBkiMydi4Lzxr6K8TPsX4S9fi_Js9rpJsLxBeCLAYZsEPIiQJ9QwYOVmcsw5vZ22EvfG4poWxNhZGyhGiZxYoguQs7vy5_5iPgewVvbE9fBG21KYMP6KerMQv6v1cuHDUIXFlXnTyEp2xt3lltdqtwxqo9N-ovKTkJBlslsuyyBVEOS5SsVw9OpyMG8hNHavhVCSNZxmIZQIX89MQ5cZ9U4VpA7E9uPrSt0dcuzEy7JWwGcU3JoBngWhVh0_]--></g></svg></div>

[Corpora](#Corpus), [Projects](#Project), and [Branches](#Branch) can
  [be complemented by](#isComplementedBy) zero or more [Complementary
  Resources](#ComplementaryResource)—[Objects](#pcdm:Object) which are
  related in some manner and aggregated together with a thing.
Like other Objects, Complementary Resources can [have
  descriptions](#hasDescription) and [have digital
  manifestations](#hasDigitalManifestation).
They can also be [Content Resources](#ContentResource) and directly
  include their [contents](#contents).

The most important kind of Complementary Resource is the [Note](#Note).
Notes are typically considered part of the branching structure of a
  Corpus, and require [identifiers](#identifier) for identification.

Other kinds of Complementary Resource include the [Cover](#Cover),
  which provides cover artwork for a thing, and
  [Soundtrack](#Soundtrack), an [Aggregation](#ore:Aggregation) of
  associated [Tracks](#Track).

{% include ontology.markdown name="BNS" numero="3" %}
